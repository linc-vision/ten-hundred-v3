import ReactDOM from 'react-dom/server'

import registration from '/server/imports/tools/email_templates/registration'
import new_user from '/server/imports/tools/email_templates/new_user'
import new_subscriber from '/server/imports/tools/email_templates/new_subscriber'
import request_created from '/server/imports/tools/email_templates/request_created'
import new_request from '/server/imports/tools/email_templates/new_request'
import request_updated from '/server/imports/tools/email_templates/request_updated'
import request_confirmation from '/server/imports/tools/email_templates/request_confirmation'
import request_confirmed from '/server/imports/tools/email_templates/request_confirmed'
import request_completed from '/server/imports/tools/email_templates/request_completed'
import subscription_canceled from '/server/imports/tools/email_templates/subscription_canceled'
import subscription_created from '/server/imports/tools/email_templates/subscription_created'



templates =
  registration: ->
    html: registration()
    subject: "Welcome to TenHundred!"

  newUser: (data) ->
    html: new_user data
    subject: "You have a new registered user!"

  newSubscriber: (data) ->
    html: new_subscriber data
    subject: "You have a new subscriber!"

  requestCreated: (data) ->
    html: request_created data
    subject: "Your recent booking request with TenHundred"

  newRequest: (data) ->
    html: new_request data
    subject: "A new Request created"

  requestUpdated: (data) ->
    html: request_updated data
    subject: "Action Required: Your request has been processed."

  requestConfirmation: (data) ->
    html: request_confirmation data
    subject: "You have confirmed your booking"

  requestConfirmed: (data) ->
    html: request_confirmed data
    subject: "A Request confirmed"

  requestCompleted: (data) ->
    html: request_completed data
    subject: "Your booking is confirmed!"

  subscriptionCanceled: ->
    html: subscription_canceled()
    subject: "TenHundred membership changed"

  subscriptionCreated: ->
    html: subscription_created()
    subject: "TenHundred membership changed"



export default
  sendEmailToUser: (address, type, data) ->
    template = templates[type](data)

    if not TEST
      Email.send
        from: 'TenHundred <hello@tenhundred.com>'
        to: address
        subject: template.subject
        html: ReactDOM.renderToString template.html

  sendEmailToAdmin: (type, data) ->
    template = templates[type](data)

    if not TEST
      Email.send
        from: 'TenHundred <hello@tenhundred.com>'
        to: 'hello@tenhundred.com'
        subject: template.subject
        html: ReactDOM.renderToString template.html
