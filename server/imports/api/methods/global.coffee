import Future from 'fibers/future'

import Screenshots from '/server/imports/db/screenshots'

import random from 'randomatic'



export default
  checkEmail: (email) ->
    future = new Future()

    if Accounts.findUserByEmail email
      future.return
        status: 'F'
        message: 'email_exist'
    else
      future.return
        status: 'S'
        message: 'Success'

    future.wait()



  saveEmail: (email) ->
    if not Accounts.findUserByEmail email
      if not Emails.findOne({ email: email })
        Emails.insert
          email: email
          member: no



  addEmail: (email, userId) ->
    future = new Future()

    if not userId then userId = Meteor.userId()

    Meteor.call 'createCustomer', email, (err, res) ->
      if err
        future.return
          status: 'F'
          message: 'Error'
      else
        user = {}

        user.emails = [
            { address: email }
          ]

        user.profile =
          customer_id: res.message.customer_id
          customer_data: res.message.customer_data
          membership: res.message.membership
          card: res.message.card

        Meteor.users.update({ _id: userId }, { $set: { 'emails': user.emails, 'profile.customer_id': user.profile.customer_id, 'profile.customer_data': user.profile.customer_data, 'profile.membership': user.profile.membership, 'profile.card': user.profile.card } })

        Meteor.call 'sendEmailToUser', email, 'registration', null
        Meteor.call 'sendEmailToAdmin', 'newUser', { email: email }

        future.return
          status: 'S'
          message: 'User profile updated.'

    future.wait()



  checkPromo: (code) ->
    future = new Future()

    if code is 'LOVESNOW'
      future.return
        status: 'S'
        message: 'Success'
    else
      future.return
        status: 'F'
        message: 'email_exist'

    future.wait()



  addPhoneNumber: (number) ->
    if Meteor.userId() isnt null
      Meteor.users.update({ _id: Meteor.userId() }, { $set: { 'profile.phone_number': number } })



  removeScreenshot: (url) ->
    if Meteor.userId() isnt null
      id = url.split('/')[url.split('/').length - 1].split('.')[0]
      Screenshots.collection.remove
        _id: id



  createNewRequest: (request) ->
    if Meteor.userId() isnt null
      data = request

      owner =
        id: Meteor.userId()
        email: Meteor.user().emails[0].address

      data.owner = owner
      data.dateCreated = new Date()
      data.status = 'processing'
      data.viewedLastAdminStatus = no
      data.viewedLastUserStatus = yes

      data.title = "#{ data.category.toUpperCase() } | #{ data.supplier.title } | $#{ data.price }"

      Requests.insert data

      Meteor.call 'sendEmailToUser', Meteor.user().emails[0].address, 'requestCreated', request
      Meteor.call 'sendEmailToAdmin', 'newRequest', Meteor.user().emails[0].address



  viewUser: (user_id) ->
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Meteor.users.update({ _id: user_id }, { $set: { 'viewedByAdmin': yes } })



  adminViewRequest: (request_id) ->
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Requests.update({ _id: request_id }, { $set: { 'viewedLastAdminStatus': yes } })



  userViewRequest: (request_id) ->
    Requests.update({ _id: request_id }, { $set: { 'viewedLastUserStatus': yes } })



  sendConfirmation: (request, data) ->
    future = new Future()

    if Roles.userIsInRole(Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP) and Requests.findOne({ _id: request._id }).status is 'processing'
      data.dateCreated = new Date()
      Requests.update({ _id: request._id }, { $set: { confirmation: data, status: 'confirmation', viewedLastUserStatus: no } })
      owner = Meteor.users.findOne({ _id: request.owner.id })
      Meteor.call 'sendEmailToUser', owner.emails[0].address, 'requestUpdated', Requests.findOne { _id: request._id }
      future.return
        status: 'S'
        message: 'Confirmation successfully sent'
    else
      future.return
        status: 'F'
        message: 'Error'

    future.wait()



  addBankAccount: (user_id, account) ->
    future = new Future()

    if Meteor.userId() isnt null and Meteor.user().profile.membership.status is 'active'
      account = {
        account...
        id: random 'Aa0', 16
        object: 'account'
      }

      Meteor.users.update({ _id: Meteor.userId() }, { $push: { 'profile.bank_accounts': account } })

      future.return
        status: 'S'
        message: 'Bank Account successfully created'
    else
      future.return
        status: 'F'
        message: 'Error'

    future.wait()



  addAddress: (user_id, address) ->
    future = new Future()

    if Meteor.userId() isnt null and Meteor.user().profile.membership.status is 'active'
      address = {
        address...
        id: random 'Aa0', 16
        object: 'address'
      }

      Meteor.users.update({ _id: Meteor.userId() }, { $push: { 'profile.addresses': address } })

      future.return
        status: 'S'
        message: 'Address successfully created'
    else
      future.return
        status: 'F'
        message: 'Error'

    future.wait()



  confirmRequest: (request_id, payment_method, cashback_method) ->
    future = new Future()

    if Meteor.userId() isnt null and Meteor.user().profile.membership.status is 'active' and Requests.findOne({ _id: request_id }).status is 'confirmation'
      Requests.update({ _id: request_id }, { $set: { 'confirmation.payment_method': payment_method, 'confirmation.cashback_method': cashback_method, dateConfirmed: new Date(), status: 'confirmed', viewedLastAdminStatus: no, viewedLastUserStatus: yes } })
      Meteor.call 'sendEmailToUser', Meteor.user().emails[0].address, 'requestConfirmation', Requests.findOne({ _id: request_id })
      Meteor.call 'sendEmailToAdmin', 'requestConfirmed', Meteor.user().emails[0].address
      future.return
        status: 'S'
        message: 'Request successfully confirmed'
    else
      future.return
        status: 'F'
        message: 'Error'

    future.wait()



  completeRequest: (request) ->
    future = new Future()

    if Roles.userIsInRole(Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP) and Requests.findOne({ _id: request._id }).status is 'confirmed'

      Requests.update({ _id: request._id }, { $set: { dateCompleted: new Date(), status: 'completed', viewedLastAdminStatus: yes, viewedLastUserStatus: yes } })

      request = Requests.findOne({ _id: request._id })
      owner = Meteor.users.findOne({ _id: request.owner.id })
      total_savings = parseInt(owner.profile.total_savings) + parseInt(request.confirmation.cashback)

      Meteor.users.update({ _id: owner._id }, { $set: { 'profile.total_savings': total_savings.toString() } })

      Meteor.call 'sendEmailToUser', owner.emails[0].address, 'requestCompleted', Requests.findOne { _id: request._id }

      future.return
        status: 'S'
        message: 'Request successfully completed'
    else
      future.return
        status: 'F'
        message: 'Error'

    future.wait()
