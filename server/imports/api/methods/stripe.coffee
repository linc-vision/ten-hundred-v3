import Future from 'fibers/future'

if TEST
  stripe = require("stripe")("sk_test_Xwa2tqXxqf2xpx17NVvcKKZ1")
else
  stripe = require("stripe")("sk_live_rWnVMHRc6LrSc5SqwDSWEEmA")

import App from '/server/imports/db/app'



export default
  createCustomer: (email) ->
    future = new Future()

    stripe.customers.create
      email: email
      description: "Customer for #{ email }"
    , Meteor.bindEnvironment (err, customer) ->
      if err
        console.log err
        future.return
          status: 'F'
          message: err
      else
        future.return
          status: 'S'
          message:
            customer_id: customer.id
            customer_data: customer
            card: []
            membership:
              status: 'inactive'
              plan:
                nickname: ''

    future.wait()



  subscribeCustomer: (email, credit_card, plan) ->
    future = new Future()

    plan = 'gold'

    stripe.customers.retrieve Meteor.user().profile.customer_id, Meteor.bindEnvironment (err, customer) ->
      if err
        console.log err
        future.return
          status: 'F'
          message: err
      else
        Meteor.call 'addCreditCard', 'new', customer.id, credit_card, Meteor.bindEnvironment (err, res) ->
          if err or res.status is 'F'
            console.log err
            future.return
              status: 'F'
              message: res.message
          else
            Meteor.call 'subscribe', 'new', customer.id, plan, (err, res_subscribe) ->
              if err or res_subscribe.status is 'F'
                stripe.customers.deleteSource customer.id, res.id
                console.log err
                future.return
                  status: 'F'
                  message: if not err then res_subscribe.message
              else
                future.return
                  status: 'S'
                  message:
                    customer_data: customer
                    card: res.message
                    membership: res_subscribe.message

    future.wait()



  addCreditCard: (type, customer_id, credit_card) ->
    future = new Future()

    credit_card.card_number = credit_card.card_number.replace(/[^0-9]+/g, '')
    credit_card.number = credit_card.card_number.replace(/[^0-9]+/g, '')
    credit_card.card_number = credit_card.card_number.replace(/[^0-9]+/g, '')

    credit_card_default = credit_card

    credit_card =
      object: 'card'
      number: credit_card.card_number
      exp_month: credit_card.exp_date.split('/')[0]
      exp_year: credit_card.exp_date.split('/')[1]
      cvc: credit_card.cvv
      currency: 'usd'
      name: credit_card.name
      address_zip: credit_card.postal_code

    stripe.customers.createSource customer_id, { source: credit_card }, Meteor.bindEnvironment (err, card) ->
      if err
        console.log err
        future.return
          status: 'F'
          message: err
      else
        credit_card_default.stripe = card
        credit_card = credit_card_default
        Meteor.users.update({ _id: Meteor.userId() }, { $push: { 'profile.credit_cards': credit_card } })
        future.return
          status: 'S'
          message: card

    future.wait()



  removeCreditCard: (credit_card_id) ->
    if Meteor.userId() isnt null
      future = new Future()

      stripe.customers.deleteCard Meteor.user().profile.customer_id, credit_card_id, Meteor.bindEnvironment (err, confirmation) ->
        if err
          console.log err
          future.return
            status: 'F'
            message: err
        else
          Meteor.users.update({ _id: Meteor.userId() }, { $pull: { 'profile.credit_cards': { 'stripe.id': credit_card_id } } })
          future.return
            status: 'S'
            message: confirmation

      future.wait()



  setDefaultCreditCard: (credit_card_id) ->
    if Meteor.userId() isnt null
      future = new Future()

      stripe.customers.update Meteor.user().profile.customer_id, { default_source: credit_card_id }, Meteor.bindEnvironment (err, customer) ->
        if err
          console.log err
          future.return
            status: 'F'
            message: err
        else
          Meteor.users.update({ _id: Meteor.userId() }, { $set: { 'profile.customer_data': customer } })
          future.return
            status: 'S'
            message: customer

      future.wait()



  subscribe: (type, customer_id, membership_plan) ->
    future = new Future()

    plan = {}

    if _.isObject membership_plan
      plan.nickname = membership_plan.plan
      plan.promo = membership_plan.promo
    else
      plan.nickname = membership_plan

    stripe.subscriptions.create
      customer: if type is 'new' then customer_id else Meteor.user().profile.customer_id
      items: [
        {
          plan: _.where(App.findOne().plans, { nickname: plan.nickname })[0].id
        }
      ]
      coupon: if plan.promo then plan.promo else null
    , Meteor.bindEnvironment (err, subscription) ->
      if err
        console.log err
        future.return
          status: 'F'
          message: err
      else
        Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.membership': subscription })
        Meteor.call 'sendEmailToUser', Meteor.user().emails[0].address, 'subscriptionCreated'
        future.return
          status: 'S'
          message: subscription

    future.wait()



  upgrade: ->
    future = new Future()

    subscription = Meteor.user().profile.membership

    if subscription.plan.nickname is 'silver'

      stripe.charges.create
        amount: 5000
        currency: "usd"
        customer: Meteor.user().profile.customer_id
        description: "Upgrade to Gold Membership"
      , Meteor.bindEnvironment (err, charge) ->
        if err
          console.log err
          future.return
            status: 'F'
            message: err
        else
          stripe.subscriptions.update subscription.id,
            cancel_at_period_end: no
            prorate: no
            items: [
              {
                id: subscription.items.data[0].id
                plan: _.findWhere(App.findOne().plans, { nickname: 'gold' }).id
              }
            ]
          , Meteor.bindEnvironment (err, subscription) ->
            if err
              console.log err
              future.return
                status: 'F'
                message: err
            else
              stripe.customers.retrieve Meteor.user().profile.customer_id, Meteor.bindEnvironment (err, customer) ->
                if err
                  console.log err
                  future.return
                    status: 'F'
                    message: err
                else
                  Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.customer_data': customer })
                  if subscription.plan.nickname is 'Silver Membership'
                    subscription.plan.nickname = 'silver'
                  else
                    subscription.plan.nickname = 'gold'
                  Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.membership': subscription })
                  future.return
                    status: 'S'

    else
      future.return
        status: 'F'
        message: 'Membership plan is Gold already.'

    future.wait()



  unsubscribe: ->
    if Meteor.userId() isnt null
      future = new Future()

      stripe.subscriptions.del Meteor.user().profile.membership.id, Meteor.bindEnvironment (err, confirmation) ->
        if err
          console.log err
          future.return
            status: 'F'
            message: err
        else
          Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.membership': confirmation })
          Meteor.call 'sendEmailToUser', Meteor.user().emails[0].address, 'subscriptionCanceled'
          future.return
            status: 'S'

      future.wait()



  checkSubscription: ->
    if Meteor.userId() isnt null
      if not Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
        future = new Future()

        stripe.customers.retrieve Meteor.user().profile.customer_id, Meteor.bindEnvironment (err, customer) ->
          if err
            console.log err
            future.return
              status: 'F'
              message: err
          else
            membership = _.findWhere customer.subscriptions.data, { status: 'active' }
            if membership
              stripe.subscriptions.retrieve membership.id, Meteor.bindEnvironment (err, subscription) ->
                if err
                  console.log err
                  future.return
                    status: 'F'
                    message: err
                else
                  stripe.customers.retrieve Meteor.user().profile.customer_id, Meteor.bindEnvironment (err, customer) ->
                    if err
                      console.log err
                      future.return
                        status: 'F'
                        message: err
                    else
                      Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.customer_data': customer })
                      if subscription.plan.nickname is 'Silver Membership'
                        subscription.plan.nickname = 'silver'
                      else
                        subscription.plan.nickname = 'gold'
                      Meteor.users.update({ _id: Meteor.userId() }, { $set: 'profile.membership': subscription })
                      future.return
                        status: 'S'
            else
              future.return
                status: 'S'

        future.wait()
