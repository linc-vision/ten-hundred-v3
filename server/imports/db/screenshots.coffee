Screenshots = {}

if Meteor.isDevelopment
  Screenshots = new FilesCollection
    collectionName: 'screenshots'
    allowClientCode: yes
    onBeforeUpload: (file) ->
      if file.size <= 10485760 and /png|jpg|jpeg/i.test file.extension
        yes
      else
        'Please upload image, with size equal or less than 10MB'
else
  Screenshots = new FilesCollection
    collectionName: 'screenshots'
    allowClientCode: yes
    storagePath: '/images'
    permissions: 0o774,
    parentDirPermissions: 0o774,
    onBeforeUpload: (file) ->
      if file.size <= 10485760 and /png|jpg|jpeg/i.test file.extension
        yes
      else
        'Please upload image, with size equal or less than 10MB'



export default Screenshots
