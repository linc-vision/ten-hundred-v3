import React from 'react'



export default (request) =>
  url = "#{ Meteor.absoluteUrl() }#{ request._id }"

  styles =
    main:
      width: '100%'
      padding: '32px 16px'
      boxSizing: 'border-box'
      textAlign: 'center'
      color: 'white'
    img:
      width: 180
      marginBottom: '1.5em'
    block:
      backgroundColor: '#0C1E38'
      padding: '64px 36px'
      boxSizing: 'border-box'
      width: '100%'
      margin: '0 auto'
      borderRadius: '10px'
    title:
      margin: 0
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      letterSpacing: 1
      fontWeight: 'bold'
    content:
      margin: '2em 0 0'
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      letterSpacing: 1
      fontWeight: 'bold'
    link:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      color: 'white'
      opacity: 0.6
      margin: '.5em 0'
    confirmation:
      position: 'relative'
      width: '100%'
      margin: '2.5em 0 0.5em'
    confirmation_title:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      position: 'relative'
      width: '100%'
      textAlign: 'center'
      textTransform: 'uppercase'
      fontSize: '110%'
    confirmation_details:
      position: 'relative'
      width: '100%'
      textAlign: 'center'
    confirmation_details_price:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      margin: '1.5em 0'
      fontSize: '100%'
    confirmation_details_price_span:
      color: '#6AA3D7'
      fontWeight: 'bold'
      letterSpacing: '0.5px'
    confirmation_details_supplier:
      margin: '1.5em 0'
    confirmation_details_supplier_label:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      fontSize: '100%'
      margin: '0 0 .3em'
    confirmation_details_supplier_data:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      fontSize: '100%'
      color: '#6AA3D7'
      margin: '0'
    confirmation_details_comment:
      margin: '1.5em 0'
    confirmation_details_comment_label:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      fontSize: '100%'
      margin: '0 0 .3em'
    confirmation_details_comment_data:
      margin: '0'
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      fontSize: '80%'
      opacity: '0.8'
      letterSpacing: '0.3px'
      whiteSpace: 'pre-line'
    confirmation_cashback:
      margin: '1.5em 0'
      textAlign: 'center'
    confirmation_cashback_h2:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      display: 'inline-block'
      margin: '0 0 6px'
      padding: '3px 6px'
      boxSizing: 'border-box'
      border: '1px solid #72BD8F'
      borderRadius: '4px'
      fontSize: '90%'
    confirmation_cashback_h2_span:
      color: '#72BD8F'
      letterSpacing: '0.3px'
    confirmation_cashback_p:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      margin: '0'
      fontSize: '70%'
      opacity: '0.6'
    footer:
      margin: '3em 0 0'
    footer_text:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      color: 'white'
      opacity: 0.6
      textDecoration: 'underline'





  <div style={ styles.main }>
    <img style={ styles.img } src='https://tenhundred.com/img/pictures/logo.png'/>
    <div style={ styles.block }>
      <h2 style={ styles.title }>Congratulations!</h2>
      <h4 style={ styles.content }>Your booking is confirmed!</h4>
      <a style={ styles.link } href={ url }>{ url }</a>
      <div id='confirmation' style={ styles.confirmation }>
        <h1 id='title' style={ styles.confirmation_title }>Details:</h1>
        <div id='details' style={ styles.confirmation_details }>
          <h2 id='price' style={ styles.confirmation_details_price }>Price: <span style={ styles.confirmation_details_price_span }>{ "$#{ request.confirmation.price }" }</span></h2>
          <div id='supplier' style={ styles.confirmation_details_supplier }>
            <h2 id='label' style={ styles.confirmation_details_supplier_label }>Supplier:</h2>
            <h2 id='data' style={ styles.confirmation_details_supplier_data }>{ request.supplier.title }</h2>
          </div>
          {
            if request.confirmation.comment.length > 0
              <div id='comment' style={ styles.confirmation_details_comment }>
                <h2 id='label' style={ styles.confirmation_details_comment_label }>Comment:</h2>
                <p id='data' style={ styles.confirmation_details_comment_data }>{ request.confirmation.comment }</p>
              </div>
          }
        </div>
        <div id='cashback' style={ styles.confirmation_cashback }>
          <h2 style={ styles.confirmation_cashback_h2 }>CASH BACK: <span style={ styles.confirmation_cashback_h2_span }>{ "you earned US$#{ request.confirmation.cashback }" }</span></h2>
          <p style={ styles.confirmation_cashback_p }>{ "Estimated #{ request.confirmation.date }" }</p>
        </div>
      </div>
      <div style={ styles.footer }>
        <a href='https://tenhundred.com' target='_blank'>
          <p style={ styles.footer_text }>www.tenhundred.com</p>
        </a>
      </div>
    </div>
  </div>
