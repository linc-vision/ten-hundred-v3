import React from 'react'



export default =>
  styles =
    main:
      width: '100%'
      padding: '32px 16px'
      boxSizing: 'border-box'
      textAlign: 'center'
      color: 'white'
    img:
      width: 180
      marginBottom: '1.5em'
    block:
      backgroundColor: '#0C1E38'
      padding: '64px 36px'
      boxSizing: 'border-box'
      width: '100%'
      margin: '0 auto'
      borderRadius: '10px'
    title:
      margin: 0
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      letterSpacing: 1
      fontWeight: 'bold'
    content:
      margin: '2em 0 0'
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      letterSpacing: 1
      fontWeight: 'bold'
    footer:
      margin: '3em 0 0'
    footer_text:
      fontFamily: 'Futura, Trebuchet MS, Arial, sans-serif'
      color: 'white'
      opacity: 0.6
      textDecoration: 'underline'



  <div style={ styles.main }>
    <img style={ styles.img } src='https://tenhundred.com/img/pictures/logo.png'/>
    <div style={ styles.block }>
      <h2 style={ styles.title }>Welcome!</h2>
      <h4 style={ styles.content }>You're TenHundred member now!</h4>
      <div style={ styles.footer }>
        <a href='https://tenhundred.com' target='_blank'>
          <p style={ styles.footer_text }>www.tenhundred.com</p>
        </a>
      </div>
    </div>
  </div>
