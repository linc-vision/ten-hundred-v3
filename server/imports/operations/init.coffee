import App from '/server/imports/db/app'

if TEST
  stripe = require("stripe")("sk_test_Xwa2tqXxqf2xpx17NVvcKKZ1")
else
  stripe = require("stripe")("sk_live_rWnVMHRc6LrSc5SqwDSWEEmA")



deleteAllCustomers = ->
  stripe.customers.list { limit: 200 }, Meteor.bindEnvironment (err, customers) ->
    if err
      console.log err
    else
      if customers.data.length > 0
        customers.data.map (customer, n) ->
          stripe.customers.del customer.id, Meteor.bindEnvironment (err, res) =>
            if err
              console.log err
            else
              if n is customers.data.length - 1
                Meteor.users.remove({})
                Meteor.roles.remove({})
                Requests.remove({})
                console.log 'All customers deleted.'
      else
        Meteor.users.remove({})
        Meteor.roles.remove({})
        Requests.remove({})
        console.log 'There are no customers. All users deleted.'

deleteAllMembershipPlans = ->
  stripe.plans.list Meteor.bindEnvironment (err, plans) ->
    if err
      console.log err
    else
      plans.data.map (plan, n) ->
        stripe.plans.del plan.id, Meteor.bindEnvironment (err, res) =>
          if err
            console.log err
          else
            if n is plans.data.length - 1
              App.update({ id: 'app' }, { $set: { plans: [] } })
              console.log 'All membership plans deleted.'



export default ->
  delete_all = no

  # stripe.plans.list Meteor.bindEnvironment (err, plans) ->
  #   console.log plans

  if delete_all
    deleteAllCustomers()
    deleteAllMembershipPlans()
  else
    if not App.findOne()

      App.insert
        id: 'app'
        plans: []
        suppliers: initital_suppliers
    else
      App.update({ id: 'app' }, { $set: { 'suppliers': initital_suppliers } })

    stripe.plans.list Meteor.bindEnvironment (err, plans) ->
      if plans.data.length is 0
        stripe.plans.create
          amount: 9900
          interval: 'year'
          currency: 'usd'
          product:
            name: 'Silver Membership'
          nickname: 'Silver Membership'
        , Meteor.bindEnvironment (err, plan) ->
          if err
            console.log 'Silver plan screating error:'
            console.log err
          else
            console.log 'Silver plan successfully created.'
            plan.nickname = 'silver'
            App.update({ id: 'app' }, { $push: { 'plans': plan } })
        stripe.plans.create
          amount: 14900
          interval: 'year'
          currency: 'usd'
          product:
            name: 'Gold Membership'
          nickname: 'Gold Membership'
        , Meteor.bindEnvironment (err, plan) ->
          if err
            console.log 'Gold plan screating error:'
            console.log err
          else
            console.log 'Gold plan successfully created.'
            plan.nickname = 'gold'
            App.update({ id: 'app' }, { $push: { 'plans': plan } })
      else
        updatedPlans = []

        silver = _.findWhere plans.data, { nickname: 'Silver Membership' }
        gold = _.findWhere plans.data, { nickname: 'Gold Membership' }

        silver.nickname = 'silver'
        gold.nickname = 'gold'

        updatedPlans.push silver
        updatedPlans.push gold

        App.update({ id: 'app' }, { $set: { 'plans': updatedPlans } })

      stripe.coupons.retrieve 'LOVESNOW', (err, res) ->
        if err
          stripe.coupons.create
            amount_off: 5000
            currency: 'usd'
            duration: 'forever'
            id: 'LOVESNOW'



initital_suppliers =
  cruises: [
    { title: 'AMA Waterways', percentage: '16%' }
    { title: 'Amadeus River Cruises', percentage: '16%' }
    { title: 'American Cruise Lines', percentage: '12%' }
    { title: 'American Queen Steamboat Company', percentage: '9%' }
    { title: 'Avalon Waterways', percentage: '16%' }
    { title: 'Azamara Club Cruises', percentage: '14%' }
    { title: 'Bahamas Paradise Cruise Line', percentage: '13%' }
    { title: 'Carnival Cruise Line', percentage: '14%' }
    { title: 'Celebrity Cruises', percentage: '13%' }
    { title: 'Celestyal Cruises', percentage: '10%' }
    { title: 'Costa Cruises', percentage: '12%' }
    { title: 'Crystal Cruises', percentage: '13%' }
    { title: 'Cunard', percentage: '13%' }
    { title: 'Disney Cruises', percentage: '14%' }
    { title: 'Emerald Waterways', percentage: '16%' }
    { title: 'Holland America', percentage: '14%' }
    { title: 'Hurtigruten', percentage: '8%' }
    { title: 'Lindblad Expeditions', percentage: '8%' }
    { title: 'MSC Cruises', percentage: '16%' }
    { title: 'Moorings Yacht Vacations', percentage: '8%' }
    { title: 'Norwegian Cruise Line', percentage: '14%' }
    { title: 'Oceania Cruises', percentage: '15%' }
    { title: 'Paul Gauguin Cruises', percentage: '12%' }
    { title: 'Ponant Yacht Cruises & Expeditions', percentage: '12%' }
    { title: 'Princess Cruises', percentage: '13%' }
    { title: 'Regent Seven Seas', percentage: '15%' }
    { title: 'Riviera River Cruises', percentage: '16%' }
    { title: 'Royal Caribbean International', percentage: '14%' }
    { title: 'Scenic Cruises', percentage: '16%' }
    { title: 'Seabourn Cruises', percentage: '13%' }
    { title: 'Seadream Yacht Club', percentage: '10%' }
    { title: 'Silversea Cruises', percentage: '10%' }
    { title: 'Star Clippers', percentage: '11%' }
    { title: 'Un-Cruise Adventures', percentage: '10%' }
    { title: 'Uniworld River Cruises', percentage: '15%' }
    { title: 'Variety Cruises', percentage: '13%' }
    { title: 'Victory Cruise Lines', percentage: '10%' }
    { title: 'Viking River Cruises', percentage: '14%' }
    { title: 'Windstar Cruises', percentage: '12%' }
    { title: 'Virgin Voyages', percentage: '10%' }
  ]
  # ski: [
  #   { region: '', resorts: '', title: 'IKON PASS', percentage: '10%' }
  #   { region: '', resorts: '', title: 'EPIC PASS', percentage: '10%' }
  #   { region: 'Western US', resorts: 'ALASKA RESORTS', title: 'Alyeska', percentage: '10%' }
  #   { region: 'Western US', resorts: 'CALIFORNIA RESORTS', title: 'Heavenly', percentage: '10%' }
  #   { region: 'Western US', resorts: 'CALIFORNIA RESORTS', title: 'Kirkwood', percentage: '10%' }
  #   { region: 'Western US', resorts: 'CALIFORNIA RESORTS', title: 'Mammoth', percentage: '10%' }
  #   { region: 'Western US', resorts: 'CALIFORNIA RESORTS', title: 'Northstar at Tahoe', percentage: '10%' }
  #   { region: 'Western US', resorts: 'CALIFORNIA RESORTS', title: 'Squaw Valley', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Aspen', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Beaver Creek', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Breckenridge', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Copper Mountain', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Crested Butte', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Durango Mountain Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Keystone', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Snowmass', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Steamboat Springs', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Summit County', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Telluride', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Vail', percentage: '10%' }
  #   { region: 'Western US', resorts: 'COLORADO RESORTS', title: 'Winter Park', percentage: '10%' }
  #   { region: 'Western US', resorts: 'IDAHO RESORTS', title: 'Schweitzer Mountain Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'IDAHO RESORTS', title: 'Sun Valley', percentage: '10%' }
  #   { region: 'Western US', resorts: 'MONTANA RESORTS', title: 'Big Sky Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'MONTANA RESORTS', title: 'Whitefish Mountain Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'NEVADA RESORTS', title: 'Reno', percentage: '10%' }
  #   { region: 'Western US', resorts: 'NEW MEXICO RESORTS', title: 'Taos', percentage: '10%' }
  #   { region: 'Western US', resorts: 'OREGON RESORTS', title: 'Mt. Bachelor', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Alta', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Deer Valley Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Park City Mountain Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Salt Lake City', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Snowbird', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Solitude Resort', percentage: '10%' }
  #   { region: 'Western US', resorts: 'UTAH RESORTS', title: 'Sundance', percentage: '10%' }
  #   { region: 'Western US', resorts: 'WYOMING RESORTS', title: 'Grand Targhee', percentage: '10%' }
  #   { region: 'Western US', resorts: 'WYOMING RESORTS', title: 'Jackson Hole', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'MAINE RESORTS', title: 'Sunday River', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'NEW HAMPSHIRE RESORTS', title: 'Loon', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'NEW YORK RESORTS', title: 'Lake Placid', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'VERMONT RESORTS', title: 'Jay Peak', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'VERMONT RESORTS', title: 'Killington', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'VERMONT RESORTS', title: 'Mount Snow', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'VERMONT RESORTS', title: 'Okemo', percentage: '10%' }
  #   { region: 'Eastern US', resorts: 'VERMONT RESORTS', title: 'Stowe', percentage: '10%' }
  #   { region: 'Canada', resorts: 'ALBERTA RESORTS', title: 'Banff/Lake Louise', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Big White', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Fernie', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Kicking Horse', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Kimberley', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Panorama', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Red Mountain', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Revelstoke', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Silver Star', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Sun Peaks', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Whistler Blackcomb', percentage: '10%' }
  #   { region: 'Canada', resorts: 'BRITISH COLUMBIA RESORTS', title: 'Whitewater', percentage: '10%' }
  #   { region: 'Canada', resorts: 'QUEBEC RESORTS', title: 'Mont Sainte Anne', percentage: '10%' }
  #   { region: 'Canada', resorts: 'QUEBEC RESORTS', title: 'Mont-Tremblant', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ANDORRA RESORTS', title: 'Andorra', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Bad Gastein', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Igls', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Innsbruck', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Kitzbuhel', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Lech', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Solden', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'St. Anton', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Zell am See', percentage: '10%' }
  #   { region: 'Europe', resorts: 'AUSTRIA RESORTS', title: 'Zurs', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Alpe D Huez', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Avoriaz', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Brides les Bains', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Chamonix', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Courchevel', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Les Menuires', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Megeve', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Meribel', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Morzine', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Tignes', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Val D Isere', percentage: '10%' }
  #   { region: 'Europe', resorts: 'FRANCE RESORTS', title: 'Val Thorens', percentage: '10%' }
  #   { region: 'Europe', resorts: 'GERMANY RESORTS', title: 'Garmisch', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Alta Badia', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Arabba Marmolada', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Bormio', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Cervinia', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Champoluc', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Cortina', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Courmayeur', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'La Thuile', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Livigno', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Madesimo', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Madonna di Campiglio', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Sauze d\'Oulx', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Sestriere', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Val Di Fassa', percentage: '10%' }
  #   { region: 'Europe', resorts: 'ITALY RESORTS', title: 'Val Gardena', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Andermatt', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Arosa', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Crans Montana', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Davos', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Engelberg-Titlis', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Flims', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Grindelwald', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Gstaad', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Interlaken', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Klosters', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Saas Fee', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'St. Moritz', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Verbier', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Wengen', percentage: '10%' }
  #   { region: 'Europe', resorts: 'SWITZERLAND RESORTS', title: 'Zermatt', percentage: '10%' }
  #   { region: 'S. America', resorts: 'ARGENTINA RESORTS', title: 'Bariloche', percentage: '10%' }
  #   { region: 'S. America', resorts: 'ARGENTINA RESORTS', title: 'Las Lenas', percentage: '10%' }
  #   { region: 'S. America', resorts: 'CHILE RESORTS', title: 'Corralco', percentage: '10%' }
  #   { region: 'S. America', resorts: 'CHILE RESORTS', title: 'Portillo', percentage: '10%' }
  #   { region: 'S. America', resorts: 'CHILE RESORTS', title: 'Termas de Chillan', percentage: '10%' }
  #   { region: 'S. America', resorts: 'CHILE RESORTS', title: 'Valle Nevado', percentage: '10%' }
  #   { region: 'Asia', resorts: 'JAPAN RESORTS', title: 'Furano', percentage: '10%' }
  #   { region: 'Asia', resorts: 'JAPAN RESORTS', title: 'Hakuba', percentage: '10%' }
  #   { region: 'Asia', resorts: 'JAPAN RESORTS', title: 'Kiroro', percentage: '10%' }
  #   { region: 'Asia', resorts: 'JAPAN RESORTS', title: 'Niseko', percentage: '10%' }
  #   { region: 'Asia', resorts: 'JAPAN RESORTS', title: 'Rusutsu', percentage: '10%' }
  # ]
