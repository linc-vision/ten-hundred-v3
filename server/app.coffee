import '/server/lib/global_scope'

import Future from 'fibers/future'
import App from './imports/db/app'
import Screenshots from './imports/db/screenshots'

import emails from './imports/api/methods/emails'
import global from './imports/api/methods/global'
import stripe from './imports/api/methods/stripe'

import init from './imports/operations/init'



@Emails = new Mongo.Collection 'emails'
@Requests = new Mongo.Collection 'requests'



Meteor.startup ->
  console.log "----------------------------------------------------------------------------"
  console.log "|                 Ten Hundred server started successfully                  |"
  console.log "----------------------------------------------------------------------------"



  process.env.MAIL_URL = 'smtps://apikey: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@smtp.sendgrid.net:465'



  Meteor.publish 'app', =>
    App.find({}, { fields: { 'suppliers': 1 } })

  Meteor.publish 'files.images.all', =>
    Screenshots.find().cursor

  Meteor.publish 'requests', =>
    if not Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Requests.find({ 'owner.id': Meteor.userId() }, { fields: { 'viewedLastAdminStatus': 0 } })
    else
      Requests.find()

  Meteor.publish 'allUsers', =>
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Meteor.users.find()
    else
      []

  Meteor.publish 'Emails', =>
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      Emails.find()
    else
      []



  init()



  if not Meteor.users.find({ username: 'tenhundred.admin' }).fetch()[0]
    Accounts.createUser
      username: 'tenhundred.admin'
      email: 'admin@tenhundred.com'
      password: 'Hellotenhundred1'

    Roles.addUsersToRoles Meteor.users.findOne({ username: 'tenhundred.admin' })._id, ['admin'], Roles.GLOBAL_GROUP



  Meteor.methods {
    emails...
    global...
    stripe...
  }




  Accounts.loginServiceConfiguration.remove
    service: "google"

  Accounts.loginServiceConfiguration.remove
    service: "facebook"

  Accounts.loginServiceConfiguration.insert
    service: "google"
    clientId: "992111559994-2a16j0i5b6pl99ergic0t5het4tdpn7k.apps.googleusercontent.com"
    secret: "gS1QBS2Sv7W8fIbCwcYq5nbc"

  Accounts.loginServiceConfiguration.insert
    service: "facebook"
    appId: "1059466807579529"
    secret: "4a5e4f3505da246610fb7035f3b38584"

  Accounts.onCreateUser (options, user) ->
    future = new Future()

    user.profile = options.profile
    user.viewedByAdmin = no

    if user.services.facebook
      viaSocial = yes
      email = user.services.facebook.email
    else if user.services.google
      viaSocial = yes
      email = user.services.google.email
    else
      viaSocial = no
      email = user.emails[0].address

    if viaSocial
      if email
        Meteor.call 'createCustomer', email, (err, res) ->
          if not err
            user.emails = [
                { address: email }
              ]
            user.profile =
              customer_id: res.message.customer_id
              customer_data: res.message.customer_data
              membership: res.message.membership
              phone_number: ''
              total_savings: '0'
              credit_cards: []
              card: res.message.card

            if Emails.findOne({ email: email })
              Emails.update({ email: email }, { $set: { member: yes } })

            Meteor.call 'sendEmailToUser', email, 'registration', null
            Meteor.call 'sendEmailToAdmin', 'newUser', { email: email }

            future.return user

      else
        user.emails = []
        user.profile =
          customer_id: null
          customer_data: null
          membership: null
          phone_number: ''
          total_savings: '0'
          credit_cards: []
          card: null

        future.return user
    else
      if Emails.findOne({ email: email })
        Emails.update({ email: email }, { $set: { member: yes } })

      Meteor.call 'sendEmailToUser', email, 'registration', null
      Meteor.call 'sendEmailToAdmin', 'newUser', { email: email }

      future.return user

    future.wait()



  Accounts.validateLoginAttempt (info) ->
    if info.type is 'facebook'
      if info.user.emails.length > 0
        yes
      else
        if not info.user.services.facebook.email
          throw new Meteor.Error 405, info.user._id
    else
      yes



  sitemaps.add '/sitemap.xml', =>
    [
      { page: '/', lastmod: new Date() }
      { page: '/cruises', lastmod: new Date() }
      { page: '/ski', lastmod: new Date() }
      { page: '/sign-in', lastmod: new Date() }
      { page: '/terms-of-service', lastmod: new Date() }
      { page: '/provacy-policy', lastmod: new Date() }
      { page: '/register', lastmod: new Date() }
      { page: '/all-requests', lastmod: new Date() }
      { page: '/my-membership', lastmod: new Date() }
      { page: '/new-request', lastmod: new Date() }
    ]
