module.exports = {
  servers: {
    one: {
      host: 'tenhundred.com',
      username: 'ubuntu',
      pem: '../../../keys/tenhundred-aws.pem'
    }
  },

  app: {
    name: 'ten-hundred',
    path: '../',

    volumes: {
      '/images':'/images'
    },

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      ROOT_URL: 'https://tenhundred.com',
      MONGO_URL: 'mongodb://mongodb/meteor',
      MONGO_OPLOG_URL: 'mongodb://mongodb/local',
    },

    docker: {
      image: 'zodern/meteor:root'
    },

    enableUploadProgressBar: true
  },

  mongo: {
    version: '3.4.1',
    servers: {
      one: {}
    }
  },

  proxy: {
    domains: 'tenhundred.com',

    ssl: {
      letsEncryptEmail: 'vlad@hexa.systems',
      forceSSL: true
    }
  }
};
