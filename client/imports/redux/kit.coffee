import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import app from './states/app'
import data from './states/data'



export default (history) ->
  combineReducers
    router: connectRouter history
    app: app
    data: data
