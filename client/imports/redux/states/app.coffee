origin =
  user:
    logged: Meteor.userId() isnt null
    profile:
      email: ''
      membership: {}
      phone_number: ''
  modal:
    opened: no
  page: {
    id: 'home'
    previous_id: ''
  }
  register_form:
    active_plan: 'gold'
    cashback:
      cruise: '14%'
      ski: '10%'
    form:
      name: ''
      card_number: ''
      cvv: ''
      exp_date: ''
      postal_code: ''



export default (state = origin, action) ->
  switch action.type

    when 'OPEN_MODAL'
      state = { state... }

      if action.payload.data
        state = {
          state...
          modal:
            opened: yes
            type: action.payload.type
            data: action.payload.data
        }
      else
        state = {
          state...
          modal:
            opened: yes
            type: action.payload
        }

      state



    when 'CLOSE_MODAL'
      state = { state... }

      state = {
        state...
        modal:
          opened: no
      }

      state



    when 'CHANGE_PAGE'
      state = { state... }

      state = {
        state...
        page: {
          id: action.payload
          previous_id: state.page.id
        }
      }

      state



    when 'SAVE_EMAIL'
      state = { state... }

      state.user.profile.email = action.payload

      Meteor.call 'saveEmail', action.payload

      state



    when 'SET_FB_SESSION'
      state = { state... }

      state.user.fb_session_userId = action.payload

      state



    when 'SELECT_MEMBERSHIP'
      state = { state... }

      if action.payload is 'gold' or action.payload is 'silver'
        state.register_form.active_plan = action.payload
        state.register_form.promo = action.payload.promo
      else
        state.register_form.active_plan = action.payload.plan
        if action.payload.plan is 'silver' and action.payload.promo.length > 0
          state.register_form.promo = action.payload.promo

      state



    when 'MODIFY_REGISTER_FORM'
      state = { state... }

      data = action.payload

      if state.register_form.promo
        data.promo = state.register_form.promo

      state = {
        state...
        register_form: data
      }

      state



    when 'SET_CUSTOMER_ID'
      state = { state... }

      state.user.profile.customer_id = action.payload

      state



    when 'SET_CUSTOMER'
      state = { state... }

      state.user.profile.customer_data = action.payload

      state



    when 'SET_STRIPE_CARD'
      state = { state... }

      state.register_form.form.stripe = action.payload

      state



    when 'SET_MEMBERSHIP'
      state = { state... }

      state.user.profile.membership = action.payload

      state



    when 'SIGN_IN'
      state = { state... }

      data = Meteor.user()

      if not Roles.userIsInRole data._id, ['admin'], Roles.GLOBAL_GROUP
        credit_cards = []

        if data.profile.credit_cards and data.profile.credit_cards.length > 0
          data.profile.credit_cards.map (card) ->
            if card.stripe
              credit_cards.push card.stripe

        data.profile.credit_cards = credit_cards

        state.user.logged = yes
        state.user.profile = data.profile
        state.user.profile.email = if data.emails.length > 0 then data.emails[0].address else '---'

        if state.user.profile.membership
          if state.user.profile.membership.plan.nickname is 'Silver Membership'
            state.user.profile.membership.plan.nickname = 'silver'
          else if state.user.profile.membership.plan.nickname is 'Gold Membership'
            state.user.profile.membership.plan.nickname = 'gold'
      else
        state.user.logged = yes
        state.user.profile.email = data.emails[0].address

      state



    when 'LOG_OUT'
      state = { state... }

      Meteor.logout()

      state.user.logged = no
      state.user.profile =
        email: ''
        membership: {}
        phone_number: ''

      state



    when 'ADD_PHONE_NUMBER'
      state = { state... }

      Meteor.call 'addPhoneNumber', action.payload

      state.user.profile.phone_number = action.payload

      state



    when 'CREATE_NEW_REQUEST'
      state = { state... }

      Meteor.call 'createNewRequest', action.payload

      state



    when 'ADMIN_SET_CURRENT_REQUEST'
      state = { state... }

      state.current_request = action.payload

      state



    when 'SET_USER_NOTIFICATIONS'
      state = { state... }

      state.notifications = action.payload

      state



    when 'SET_ADMIN_NOTIFICATIONS'
      state = { state... }

      state.notifications = action.payload

      state



    else state
