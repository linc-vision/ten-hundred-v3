origin =
  cruises: []
  ski: []
  requests: []



export default (state = origin, action) ->
  switch action.type

    when 'SET_CRUISES'
      state = { state... }

      state.cruises = action.payload

      state



    when 'SET_SKI'
      state = { state... }

      state.ski = action.payload

      state



    when 'SET_REQUESTS'
      state = { state... }

      state.requests = action.payload

      state



    when 'SET_USERS'
      state = { state... }

      if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
        state.users = action.payload

      state



    when 'SET_EMAILS'
      state = { state... }

      if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
        state.emails = action.payload

      state



    else state
