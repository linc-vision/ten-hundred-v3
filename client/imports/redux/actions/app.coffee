export default (transfer, props) ->

  openModal: (type, data) ->
    transfer (transfer, getState) ->
      state = getState()

      if data
        data =
          type: type
          data: data
        transfer
          type: 'OPEN_MODAL'
          payload: data
      else
        transfer
          type: 'OPEN_MODAL'
          payload: type



  closeModal: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'CLOSE_MODAL'



  changePage: (page) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'CHANGE_PAGE'
        payload: page



  saveEmail: (email) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SAVE_EMAIL'
        payload: email



  setFBSession: (userId) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_FB_SESSION'
        payload: userId



  selectMembership: (plan, promo) ->
    transfer (transfer, getState) ->
      state = getState()

    if plan is 'silver' and promo.length > 0
      transfer
        type: 'SELECT_MEMBERSHIP'
        payload:
          plan: plan
          promo: promo
    else
      transfer
        type: 'SELECT_MEMBERSHIP'
        payload: plan



  modifyRegisterForm: (data) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'MODIFY_REGISTER_FORM'
        payload: data



  setCustomerId: (id) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_CUSTOMER_ID'
        payload: id



  setCustomer: (data) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_CUSTOMER'
        payload: data



  setStripeCard: (card) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_STRIPE_CARD'
        payload: card



  setMembership: (membership) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_MEMBERSHIP'
        payload: membership



  signIn: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SIGN_IN'



  logOut: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'LOG_OUT'



  addPhoneNumber: (number) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'ADD_PHONE_NUMBER'
        payload: number



  createNewRequest: (request) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'CREATE_NEW_REQUEST'
        payload: request



  adminSetCurrentRequest: (request) ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'ADMIN_SET_CURRENT_REQUEST'
        payload: request



  adminUnsetCurrentRequest: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'ADMIN_UNSET_CURRENT_REQUEST'



  setUserNotifications: ->
    transfer (transfer, getState) ->
      state = getState()

      notifications =
        all_requests: _.filter(state.data.requests, (request) -> request.viewedLastUserStatus is no).length

      transfer
        type: 'SET_USER_NOTIFICATIONS'
        payload: notifications



  setAdminNotifications: ->
    transfer (transfer, getState) ->
      state = getState()

      new_requests = _.filter(state.data.requests, (request) -> request.viewedLastAdminStatus is no)

      notifications =
        requests:
          processing: _.filter(new_requests, (request) -> request.status is 'processing').length
          confirmed: _.filter(new_requests, (request) -> request.status is 'confirmed').length
        subscribers: _.filter(state.data.users, (user) -> user.viewedByAdmin is no).length

      transfer
        type: 'SET_ADMIN_NOTIFICATIONS'
        payload: notifications
