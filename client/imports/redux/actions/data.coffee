App = new Mongo.Collection 'app'
Meteor.subscribe 'app'

Requests = new Mongo.Collection 'requests'
Meteor.subscribe 'requests'

Meteor.subscribe 'allUsers'
Emails = new Mongo.Collection 'emails'
Meteor.subscribe 'Emails'



export default (transfer, props) ->

  setCruises: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_CRUISES'
        payload: if App.findOne() then App.findOne().suppliers.cruises else []



  setSki: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_SKI'
        payload:  if App.findOne() then App.findOne().suppliers.ski else []



  setRequests: ->
    transfer (transfer, getState) ->
      state = getState()

      transfer
        type: 'SET_REQUESTS'
        payload: Requests.find({}, { sort: { dateCreated: -1 } }).fetch()



  setUsers: ->
    if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
      transfer (transfer, getState) ->
        state = getState()

        transfer
          type: 'SET_USERS'
          payload: Meteor.users.find({ _id: { $not: Meteor.userId() } }, { sort: { dateCreated: -1 } }).fetch()

        transfer
          type: 'SET_EMAILS'
          payload: Emails.find({ member: no }, { sort: { dateCreated: -1 } }).fetch()
