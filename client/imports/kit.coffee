import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import MessengerCustomerChat from 'react-messenger-customer-chat'

import { outfit } from '/client/imports/redux/outfit'

import { Left, Right, Modal, MobileHome, MobileRegister, MobileCreatePassword, MobileSignIn, NewRequest, AllRequests, Request, MyMembership, TermsOfService, PrivacyPolicy, VideoBg } from './ui/parts/kit'

import Animations from '/client/imports/ui/animations/kit'



App = class extends Component
  constructor: (props) ->
    super props
    @state =
      ready: no
      loaded: no



  componentDidMount: =>
    gtag 'js', new Date()
    gtag 'config', 'AW-755287007'

    Tracker.autorun =>
      if Accounts.loginServicesConfigured()
        if @props.app.user.logged
          @props.app.signIn()
          Meteor.call 'checkSubscription', (err, res) =>
            if not err
              @props.app.signIn()
            if not @state.ready
              @showApp()
        else
          @showApp()
      @props.data.setCruises()
      @props.data.setSki()
      @props.data.setRequests()
      if Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
        @props.data.setUsers()
        @props.app.setAdminNotifications()
      else
        @props.app.setUserNotifications()



  showApp: =>
    setTimeout () =>
      @setState
        ready: yes
      , =>
        if @props.app.page.previous_id is ''
          Animations.global.showPage =>
            @setState
              loaded: yes
        else
          @setState
            loaded: yes
    , 400

  logout: =>
    Animations.global.changePage =>
      @props.app.logOut()
      @props.history.push '/home'



  render: =>
    <div id='App'>
      {
        if not @state.ready
          <div id='logo' className={ if IS.mobile() then 'mobile' }>
            <img src='/img/pictures/logo_white.png' alt=''/>
            <img src='/img/pictures/video_bg.jpg' style={{ position: 'absolute', top: 0, left: 0, width: '100vw', height: '100vh', pointerEvents: 'none', opacity: 0, zIndex: '-999' }}/>
          </div>
        else
          if IS.mobile()
            <div id='layout' className={ if not @state.loaded then 'hidden mobile' else 'mobile' }>
              {
                if @props.app.user.logged
                  if not Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
                    if @props.app.user.profile.membership.status is 'active'
                      <Switch>
                        <Route path='/new-request' render={ => <NewRequest/> }/>
                        <Route path='/all-requests' render={ => <AllRequests/> }/>
                        <Route path='/my-membership' render={ => <MyMembership/> }/>
                        <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                        <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                        <Route path='/:request' render={ => <Request/> }/>
                        <Redirect from='*' to='/all-requests'/>
                      </Switch>
                    else
                      <Switch>
                        <Route path='/new-request' render={ => <NewRequest/> }/>
                        <Route path='/all-requests' render={ => <AllRequests/> }/>
                        <Route path='/my-membership' render={ => <MyMembership/> }/>
                        <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                        <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                        <Redirect from='*' to='/my-membership'/>
                      </Switch>
                  else
                    <div id='no_mobile'>
                      <h2>The mobile version of the Admin Panel hasn't yet been developed. Please, use desktop version instead.</h2>
                      <p id='log_out' className='cursor_pointer' onClick={ @logout }>log out</p>
                    </div>
                else
                  <Switch>
                    <Route exact path='/' render={ => <MobileHome/> }/>
                    <Route path='/sign-in' render={ => <MobileHome/> }/>
                    <Route path='/cruises' render={ =>
                      <MobileHome popup='cruises'/>
                    }/>
                    <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                    <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                    <Redirect from='*' to='/'/>
                  </Switch>
              }
              {
                if @props.app.modal.opened
                  <Modal/>
              }
              <MessengerCustomerChat pageId='2309631669310814' appId='1059466807579529' themeColor='#6AA3CF' loggedInGreeting='Welcome to TenHundred! How can we help?' loggedOutGreeting='Welcome to TenHundred! How can we help?'/>
            </div>
          else
            <div id='layout' className={ if not @state.loaded then 'hidden' }>
              <VideoBg/>
              <Left/>
              <Right/>
              {
                if @props.app.modal.opened
                  <Modal/>
              }
              <MessengerCustomerChat pageId='2309631669310814' appId='1059466807579529' themeColor='#6AA3CF' loggedInGreeting='Welcome to TenHundred! How can we help?' loggedOutGreeting='Welcome to TenHundred! How can we help?'/>
            </div>
      }
    </div>



export default outfit App
