import anime from 'animejs'
import $ from 'jquery'



default_timing = 300



export default

  showModal: ->
    anime
      targets: '#Modal'
      duration: default_timing / 3
      easing: 'easeInOutQuint'
      opacity: [0, 1]
      complete: ->
        anime
          targets: '#Modal > #board'
          duration: default_timing * 2
          easing: 'easeInOutQuint'
          opacity: [0, 1]
          translateX: if not IS.mobile() then ['-50%', '-50%']
          translateY: if not IS.mobile() then ['-50%', '-50%']
          scale: [0.8, 1]
          rotateX: [-10, 0]




  hideModal: (callback) ->
    anime
      targets: '#Modal > #board'
      duration: default_timing
      easing: 'easeInOutQuint'
      opacity: [1, 0]
      scale: [1, 0.8]
      rotateX: [0, 10]
      complete: ->
        callback()

    anime
      targets: '#Modal'
      duration: default_timing * 1.5
      easing: 'easeInOutQuint'
      opacity: [1, 0]



  changePage: (changeContent) ->
    if not IS.mobile()
      anime
        targets: '#Left'
        duration: default_timing * 2
        easing: 'easeInOutQuint'
        translateX: [0, '200%']
        complete: ->
          changeContent()
          anime
            targets: '#Left'
            duration: 0
            translateX: ['200%', '-125%']
            complete: ->
              anime
                targets: '#Left'
                duration: default_timing
                easing: 'easeInOutSine'
                translateX: ['-125%', 0]

      anime
        targets: '#Right'
        duration: default_timing
        easing: 'easeInOutQuint'
        delay: default_timing / 2
        opacity: [1, 0]
        complete: ->
          anime
            targets: '#Right'
            duration: default_timing * 2
            easing: 'easeInOutQuint'
            delay: default_timing / 2
            opacity: [0, 1]
    else
      anime
        targets: '#App > #layout.mobile'
        duration: default_timing
        easing: 'easeInOutQuint'
        translateX: [0, '100%']
        complete: ->
          $('#App > #layout').animate
            scrollTop: 0
          , 300
          changeContent()
          anime
            targets: '#App > #layout.mobile'
            duration: 0
            translateX: ['100%', '-100%']
            complete: ->
              anime
                targets: '#App > #layout.mobile'
                duration: default_timing / 2
                easing: 'easeInOutSine'
                translateX: ['-100%', 0]



  showPage: (showContent) ->
    showContent()

    if not IS.mobile()
      anime
        targets: '#Left'
        duration: default_timing
        easing: 'easeInOutSine'
        translateX: ['-100%', 0]

      anime
        targets: '#Right'
        duration: default_timing * 2
        easing: 'easeInOutQuint'
        delay: default_timing / 2
        opacity: [0, 1]
    else
      anime
        targets: '#App > #layout.mobile'
        duration: default_timing
        easing: 'easeInOutSine'
        translateX: ['-100%', 0]

      anime
        targets: '#App > #layout.mobile'
        duration: default_timing * 2
        easing: 'easeInOutQuint'
        delay: default_timing / 2
        opacity: [0, 1]



  toggleForm: (changeForm) ->
    if not IS.mobile()
      anime
        targets: '#Landing > #form > #right'
        duration: default_timing * 2
        easing: 'easeInOutQuint'
        opacity: [1, 0]
        scale: [1, 0.7]
        complete: ->
          changeForm()
          anime
            targets: '#Landing > #form > #right'
            duration: default_timing * 2
            opacity: [0, 1]
            scale: [0.7, 1]
    else
      anime
        targets: '#MobileHome #EmailForm #layout, #MobileHome #SignIn #layout, #MobileHome #CreatePasswordForm #layout'
        duration: default_timing
        easing: 'easeInOutQuint'
        opacity: [1, 0]
        scale: [1, 0.7]
        complete: ->
          changeForm()
          anime
            targets: '#MobileHome #EmailForm #layout, #MobileHome #SignIn #layout, #MobileHome #CreatePasswordForm #layout'
            duration: default_timing * 2
            opacity: [0, 1]
            scale: [0.7, 1]
