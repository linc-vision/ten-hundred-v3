import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import { VideoBg } from '../kit'

import './kit.styl'



CreatePasswordForm = class extends Component
  constructor: (props) ->
    super props
    @state =
      password: ''
      passwordValid: null
      confirm_password: ''
      confirm_passwordValid: null
      response:
        status: ''
        message: ''



  typePassword: (e) =>
    @setState
      password: e.target.value

  validatePassword: =>
    @setState
      passwordValid: @state.password.length > 5

  typeConfirmPassword: (e) =>
    @setState
      confirm_password: e.target.value

  validateConfirmPassword: =>
    @setState
      confirm_passwordValid: @state.passwordValid is yes and @state.confirm_password is @state.password

  validateAll: =>
    @setState
      passwordValid: @state.password.length > 5
      confirm_passwordValid: @state.passwordValid is yes and @state.confirm_password is @state.password

  keyPress: (e) =>
    if e.key is 'Enter'
      @submit()

  submit: =>
    if @props.app.user.profile.email.length > 0
      @validateAll()
      setTimeout =>
        if @state.passwordValid and @state.confirm_passwordValid
          @setState
            response:
              status: 'loading'
              message: ''
          , =>
            Meteor.call 'createCustomer', @props.app.user.profile.email, (err, res) =>
              if err or res.status is 'F'
                if res.status is 'F'
                  @setState
                    response:
                      status: 'ready'
                      message: res.message
                  , =>
                    setTimeout () =>
                      if @state.response.status isnt '' and @state.response.message isnt ''
                        @hideResponse()
                    , 8000
                else
                  @setState
                    response:
                      status: 'ready'
                      message: 'Server Error. Please try again later.'
                    setTimeout () =>
                      if @state.response.status isnt '' and @state.response.message isnt ''
                        @hideResponse()
                    , 8000
              else
                if res.status is 'S'
                  @props.app.setCustomerId res.message.customer_id
                  @props.app.setCustomer res.message.customer_data
                  @props.app.setStripeCard res.message.card
                  @props.app.setMembership res.message.membership

                  data =
                    email: @props.app.user.profile.email
                    password: @state.password
                    profile:
                      customer_id: @props.app.user.profile.customer_id
                      customer_data: @props.app.user.profile.customer_data
                      membership: @props.app.user.profile.membership
                      phone_number: ''
                      total_savings: '0'
                      credit_cards: []

                  Accounts.createUser data, (err, res) =>
                    if err
                      console.log err
                    else
                      Animations.global.changePage =>
                        @props.app.signIn()
                        analytics.track "A new user registered"
                        gtag 'event', 'conversion', { 'send_to': 'AW-755287007/svJ9CM7t9JgBEN-Hk-gC' }
                        @props.history.push '/new-request'
      , 100
    else
      Animations.global.changePage =>
        @props.history.push '/'

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''



  render: =>
    <div id='CreatePasswordForm' className={ if IS.mobile() then 'mobile' }>
      {
        if @props.mobile
          <div id='video_bg'>
            <VideoBg/>
          </div>
      }
      <div id='layout'>
        <div id='form'>
          {
            if @state.response.status isnt ''
              <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                {
                  if @state.response.status is 'ready'
                    <p>{ @state.response.message }</p>
                  else
                    <div id='loading'></div>
                }
              </div>
          }
          <h1 className={ classNames 'transparent': @state.response.status isnt '' }>Create your password:</h1>
          <input className={ classNames 'right_form', 'invalid': @state.passwordValid is no, 'transparent': @state.response.status isnt '' } type='password' name='password' placeholder='Your password' value={ @state.password } onChange={ @typePassword } onBlur={ @validatePassword }/>
          <input className={ classNames 'right_form', 'invalid': @state.confirm_passwordValid is no, 'transparent': @state.response.status isnt '' } type='password' name='password' placeholder='Confirm your password' value={ @state.confirm_password } onChange={ @typeConfirmPassword } onBlur={ @validateConfirmPassword } onKeyPress={ @keyPress }/>
          <button className={ classNames 'button_blue', 'transparent': @state.response.status isnt '' } onClick={ @submit }>Login</button>
          <p id='error' className={ classNames 'hidden': @state.passwordValid isnt no and @state.confirm_passwordValid isnt no }>{ if not @state.passwordValid then "Please enter at least 6 characters" else "Fields do not match" }</p>
        </div>
      </div>
    </div>



export default outfit CreatePasswordForm
