import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import './kit.styl'



ModalAlertUpgrade = class extends Component
  constructor: (props) ->
    super props
    @state =
      response:
        status: ''
        message: ''



  upgradeMembership: =>
    @setState
      response:
        status: 'loading'
        message: ''
    , =>
      Meteor.call 'upgrade', (err, res) =>
        if err or res.status is 'F'
          @setState
            response:
              status: 'ready'
              message: res.message
          , =>
            console.log err
            @props.closeModal()
            setTimeout () =>
              if @state.response.status isnt '' and @state.response.message isnt ''
                @props.closeModal()
            , 4000
        else
          @props.app.signIn()
          analytics.track 'Subscriber upgraded Membership plan'
          @props.closeModal()



  render: =>
    <div id='ModalAlertUpgrade'>
      <div id='layout'>
        <h2 className={ classNames 'transparent': @state.response.status isnt '' }>Would you like to proceed?</h2>
        <p className={ classNames 'transparent': @state.response.status isnt '' }>{ "You will be charged $50 for the upgrade and your gold membership will renew on #{ moment(@props.app.user.profile.membership.current_period_end * 1000).format 'DD.MM.YYYY' }."}</p>
        <button className={ classNames 'button_gold', 'transparent': @state.response.status isnt '' } onClick={ @upgradeMembership }>Upgrade</button>
        <button className={ classNames 'button_grey', 'transparent': @state.response.status isnt '' } onClick={ @props.closeModal }>Cancel</button>
        {
          if @state.response.status isnt ''
            <div id='response' onClick={ if @state.response.status is 'ready' then @props.closeModal() }>
              {
                if @state.response.status is 'ready'
                  <p>{ @state.response.message }</p>
                else
                  <div id='loading'></div>
              }
            </div>
        }
      </div>
    </div>



export default outfit ModalAlertUpgrade
