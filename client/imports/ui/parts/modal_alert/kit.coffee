import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import './kit.styl'



ModalAlert = class extends Component
  constructor: (props) ->
    super props
    @state =
      response:
        status: ''
        message: ''



  cancelMembership: =>
    @setState
      response:
        status: 'loading'
        message: ''
    , =>
      Meteor.call 'unsubscribe', (err, res) =>
        if err or res.status is 'F'
          @setState
            response:
              status: 'ready'
              message: res.message
          , =>
            console.log err
            @props.closeModal()
            setTimeout () =>
              if @state.response.status isnt '' and @state.response.message isnt ''
                @props.closeModal()
            , 4000
        else
          @props.app.signIn()
          analytics.track 'Subscriber canceled Membership'
          @props.closeModal()



  render: =>
    <div id='ModalAlert'>
      <div id='layout'>
        <h2 className={ classNames 'transparent': @state.response.status isnt '' }>Are you sure?</h2>
        <button className={ classNames 'button_grey', 'transparent': @state.response.status isnt '' } onClick={ @cancelMembership }>Submit</button>
        <button className={ classNames 'button_blue', 'transparent': @state.response.status isnt '' } onClick={ @props.closeModal }>Cancel</button>
        {
          if @state.response.status isnt ''
            <div id='response' onClick={ if @state.response.status is 'ready' then @props.closeModal() }>
              {
                if @state.response.status is 'ready'
                  <p>{ @state.response.message }</p>
                else
                  <div id='loading'></div>
              }
            </div>
        }
      </div>
    </div>



export default outfit ModalAlert
