import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import $ from 'jquery'

import classNames from 'classnames'

import './kit.styl'



AllRequests = class extends Component
  constructor: (props) ->
    super props



  componentDidMount: =>
    @props.app.changePage 'all_requests'
    analytics.track 'Subscriber viewed "All Requests" page'

    $('#App > #layout').animate
      scrollTop: 0



  goToCreateRequest: =>
    @props.history.push '/new-request'

  openRequest: (request) =>
    if @props.app.user.profile.membership.status is 'active'
      @props.history.push "/#{ request._id }"
    else
      @props.app.openModal 'membership', request



  render: =>
    <div id='AllRequests' className={ classNames 'mobile': IS.mobile() }>
      {
        if @props.app.user.profile.email isnt '---'
          <div id='layout'>
            <div id='header'>
              <div id='logo'>
                <img src='./img/pictures/logo.png' alt='Ten Hundred Logo'/>
              </div>
              {
                if IS.mobile()
                  <div id='menu_button' onClick={ @props.app.openModal.bind this, 'mobile_menu' }>
                    <div id='icon'></div>
                    <div id='notification' className={ classNames 'hidden': @props.app.notifications.all_requests is 0 }>
                      {
                        if @props.app.notifications.all_requests <= 99
                          <p>{ if @props.app.notifications.all_requests > 0 then @props.app.notifications.all_requests }</p>
                        else
                          <p>99+</p>
                      }
                    </div>
                  </div>
              }
              <div id='total_saving'>
                {
                  if @props.app.user.profile.membership.plan.nickname is 'silver'
                    if parseInt(@props.app.user.profile.total_savings) >= 900 and parseInt(@props.app.user.profile.total_savings) < 1000
                      <p className='near'>Your savings are nearing the limit</p>
                    else if parseInt(@props.app.user.profile.total_savings) >= 1000
                      <p className='full'>Your savings at the limit</p>
                }
                <h2 id='label'>Total savings:</h2>
                <h2 id='amount' className={ classNames 'full': @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 1000, 'near': @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 900 }>{ "$#{ @props.app.user.profile.total_savings }" }</h2>
              </div>
            </div>
            <div id='list'>
              {
                if @props.data.requests.length > 0
                  @props.data.requests.map (element, n) =>
                    <div key={ n } className='element'>
                      <div id='left'>
                        <div id='icon' className={ element.category }></div>
                      </div>
                      <div id='center'>
                        <h2>{ element.title }</h2>
                        {
                          if element.status isnt 'processing'
                            <div className='info'>
                              <p>amount</p>
                              <h2>{ "$#{ element.confirmation.price }" }</h2>
                            </div>
                        }
                        {
                          if element.status isnt 'processing'
                            <div className='info'>
                              <p>cashback</p>
                              <h2>{ "$#{ element.confirmation.cashback }" }</h2>
                            </div>
                        }
                        {
                          if element.status is 'processing'
                            <p id='date'>{ moment(element.dateCreated).format 'Do MMMM, YYYY' }</p>
                          else
                            <div className='info'>
                              <p>est. date</p>
                              <h2>{ element.confirmation.date }</h2>
                            </div>
                        }
                      </div>
                      <div id='right'>
                        <div id='status' className={ element.status }>
                          {
                            if element.status is 'processing'
                              if not IS.mobile()
                                <p>Request<br/>processing</p>
                              else
                                <p>Request processing</p>
                            else if element.status is 'confirmation'
                              if not IS.mobile()
                                <p>Pending<br/>confirmation</p>
                              else
                                <p>Pending confirmation</p>
                            else if element.status is 'confirmed'
                              if not IS.mobile()
                                <p>Booking<br/>confirmed</p>
                              else
                                <p>Booking confirmed</p>
                            else if element.status is 'completed'
                              <p>Completed</p>
                          }
                        </div>
                        {
                          if element.status is 'confirmation' or element.status is 'confirmed' or element.status is 'completed'
                            <button className={ classNames 'button_blue', element.status } onClick={ @openRequest.bind this, element }>Open</button>
                        }
                      </div>
                    </div>
                else
                  <div id='empty'>
                    <h2>You have no requests yet</h2>
                    <button className='button_blue' onClick={ @goToCreateRequest }>Create Your First Request</button>
                  </div>
              }
            </div>
          </div>
      }
    </div>



export default outfit AllRequests
