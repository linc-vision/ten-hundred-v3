import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'

import './kit.styl'



AdminRequests = class extends Component
  constructor: (props) ->
    super props
    @state =
      show_completed: no



  componentDidMount: =>
    if not @props.match.params.user
      @props.app.changePage 'requests'

    $('#App > #layout').animate
      scrollTop: 0



  toggleShowCompleted: =>
    @setState
      show_completed: if @state.show_completed then no else yes

  openRequest: (request) =>
    @props.app.adminSetCurrentRequest request
    @props.app.openModal 'admin_request'



  render: =>
    <div id='AdminRequests'>
      <div id='layout'>
        <div id='header'>
          <div id='processing' className='stats'>
            <h1 id='data'>{ _.where(@props.requests, { status: 'processing' }).length }</h1>
            <h2 id='label'>Waiting to<br/>be processed</h2>
          </div>
          <div id='confirmation' className='stats'>
            <h1 id='data'>{ _.where(@props.requests, { status: 'confirmation' }).length }</h1>
            <h2 id='label'>Waiting to<br/>be confirmed</h2>
          </div>
          <div id='confirmed' className='stats'>
            <h1 id='data'>{ _.where(@props.requests, { status: 'confirmed' }).length }</h1>
            <h2 id='label'>User<br/>confirmed</h2>
          </div>
          <div id='completed' className='stats'>
            <h1 id='data'>{ _.where(@props.requests, { status: 'completed' }).length }</h1>
            <h2 id='label'>Requests<br/>completed</h2>
          </div>
          <div id='total' className='stats'>
            <h1 id='data'>{ @props.requests.length }</h1>
            <h2 id='label'>Total<br/>requests</h2>
          </div>
          <div id='show_completed'>
            <div id='checkbox' className={ classNames 'cursor_pointer', 'active': @state.show_completed } onClick={ @toggleShowCompleted }>
              <div id='circle'></div>
            </div>
            <h2 id='label'>Show<br/>completed</h2>
          </div>
        </div>
        <div id='list'>
          {
            if @props.requests.length > 0
              if @state.show_completed or _.filter(@props.requests, (request) => request.status isnt 'completed').length > 0
                @props.requests.map (element, n) =>
                  if element.status isnt 'completed' or @state.show_completed
                    <div key={ n } className='element'>
                      <div id='top'>
                        <div id='left'>
                          <div id='icon' className={ element.category }></div>
                        </div>
                        <div id='center'>
                          <h2>{ element.title }</h2>
                          <p id='email'>{ element.owner.email }</p>
                          <div className='info'>
                            <p>Date created</p>
                            <h2>{ moment(element.dateCreated).format 'hh.MMa' }<br/>{ moment(element.dateCreated).format 'D.MM.YYYY' }</h2>
                          </div>
                          {
                            if element.status is 'confirmation' or element.status is 'confirmed' or element.status is 'completed'
                              <div className='info confirmation'>
                                <p>Date responded</p>
                                <h2>{ moment(element.confirmation.dateCreated).format 'hh.MMa' }<br/>{ moment(element.confirmation.dateCreated).format 'D.MM.YYYY' }</h2>
                              </div>
                          }
                          {
                            if element.status is 'confirmed' or element.status is 'completed'
                              <div className='info confirmed'>
                                <p>Date confirmed</p>
                                <h2>{ moment(element.confirmation.dateConfirmed).format 'hh.MMa' }<br/>{ moment(element.confirmation.dateConfirmed).format 'D.MM.YYYY' }</h2>
                              </div>
                          }
                        </div>
                        <div id='right'>
                          <div id='status' className={ element.status }>
                            {
                              if element.status is 'processing'
                                <p>Request waiting<br/>to be processed</p>
                              else if element.status is 'confirmation'
                                <p>Details sent. Waiting<br/>to be confirmed</p>
                              else if element.status is 'confirmed'
                                <p>User confirmed<br/>Ready to book</p>
                              else if element.status is 'completed'
                                <p>Completed</p>
                            }
                          </div>
                        </div>
                      </div>
                      {
                        if element.status isnt 'confirmation'
                          <div id='bottom'>
                            <button className={ classNames 'button_blue', element.status } onClick={ @openRequest.bind this, element }>Open</button>
                          </div>
                      }
                    </div>
              else
                <div id='empty'>
                  <h2>There are no new requests</h2>
                </div>
            else
              <div id='empty'>
                <h2>There are no requests yet</h2>
              </div>
          }
        </div>
      </div>
    </div>



export default outfit AdminRequests
