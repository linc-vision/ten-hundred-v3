import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import { NewRequest, AllRequests, Request, MyMembership, Landing, TermsOfService, PrivacyPolicy, AdminRequests, AdminSubscribers, AdminSubscribersOne } from '../kit'

import './kit.styl'



Left = class extends Component
  constructor: (props) ->
    super props



  render: =>
    <div id='Left' className={ classNames 'dashboard': @props.app.user.logged, 'admin': Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP }>
      <div id='layout'>
        {
          if @props.app.user.logged
            if not Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
              if @props.app.user.profile.membership and @props.app.user.profile.membership.status is 'active'
                <Switch>
                  <Route path='/new-request' render={ => <NewRequest/> }/>
                  <Route path='/all-requests' render={ => <AllRequests/> }/>
                  <Route path='/my-membership' render={ => <MyMembership/> }/>
                  <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                  <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                  <Route path='/:request' render={ => <Request/> }/>
                  <Redirect from='*' to='/new-request'/>
                </Switch>
              else
                if @props.app.user.profile.email is '---'
                  <Switch>
                    <Route path='/my-membership' render={ => <MyMembership/> }/>
                    <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                    <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                    <Redirect from='*' to='/my-membership'/>
                  </Switch>
                else
                  <Switch>
                    <Route path='/new-request' render={ => <NewRequest/> }/>
                    <Route path='/all-requests' render={ => <AllRequests/> }/>
                    <Route path='/my-membership' render={ => <MyMembership/> }/>
                    <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
                    <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
                    <Redirect from='*' to='/my-membership'/>
                  </Switch>
            else
              <Switch>
                <Route path='/requests' render={ => <AdminRequests requests={ @props.data.requests }/> }/>
                <Route exact path='/subscribers' render={ => <AdminSubscribers/> }/>
                <Route path='/subscribers/:user' render={ => <AdminSubscribersOne/> }/>
                <Redirect from='*' to='/requests'/>
              </Switch>
          else
            <Switch>
              <Route exact path='/' render={ => <Landing/> }/>
              <Route path='/cruises' render={ =>
                <Landing popup='cruises'/>
              }/>
              <Route path='/ski' render={ =>
                <Landing popup='ski'/>
              }/>
              <Route path='/terms-of-service' render={ => <TermsOfService/> }/>
              <Route path='/privacy-policy' render={ => <PrivacyPolicy/> }/>
              <Route path='/sign-in' render={ => <Landing/> }/>
              <Redirect from='*' to='/'/>
            </Switch>
        }
      </div>
    </div>



export default outfit Left
