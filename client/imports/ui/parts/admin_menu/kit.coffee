import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



AdminMenu = class extends Component
  constructor: (props) ->
    super props
    @state =
      notifications:
        requests:
          processing: if @props.app.notifications.requests then @props.app.notifications.requests.processing else 0
          confirmed: if @props.app.notifications.requests then @props.app.notifications.requests.confirmed else 0
        subscribers: @props.app.notifications.subscribers or 0



  componentDidUpdate: (prevProps) =>
    if @props.app.notifications
      if not _.isEqual @props.app.notifications, prevProps.app.notifications
        @setState
          notifications:
            requests:
              processing: @props.app.notifications.requests.processing or 0
              confirmed: @props.app.notifications.requests.confirmed or 0
            subscribers: @props.app.notifications.subscribers or 0



  openPage: (page) =>
    if page is 'requests'
      @props.history.push '/requests'
    else if page is 'subscribers'
      @props.history.push '/subscribers'



  render: =>
    <div id='AdminMenu'>
      <div id='layout'>
        <div id='menu'>
          <div id='requests' className={ classNames 'element', 'cursor_pointer': @props.app.page.id isnt 'requests', 'active': @props.app.page.id is 'requests' } onClick={ if @props.app.page.id isnt 'requests' then @openPage.bind this, 'requests' }>
            <div id='icon'></div>
            <h2>Requests</h2>
            <div id='processing' className={ classNames 'notification', 'hidden': @state.notifications.requests.processing is 0 }>
              {
                if @state.notifications.requests.processing <= 99
                  <p>{ if @state.notifications.requests.processing > 0 then @state.notifications.requests.processing }</p>
                else
                  <p>99+</p>
              }
            </div>
            <div id='confirmed' className={ classNames 'notification', 'top': @state.notifications.requests.processing is 0, 'hidden': @state.notifications.requests.confirmed is 0 }>
              {
                if @state.notifications.requests.confirmed <= 99
                  <p>{ if @state.notifications.requests.confirmed > 0 then @state.notifications.requests.confirmed }</p>
                else
                  <p>99+</p>
              }
            </div>
          </div>
          <div id='subscribers' className={ classNames 'element', 'cursor_pointer': @props.app.page.id isnt 'subscribers', 'active': @props.app.page.id is 'subscribers' } onClick={ if @props.app.page.id isnt 'subscribers' then @openPage.bind this, 'subscribers' }>
            <div id='icon'></div>
            <h2>Subscribers</h2>
            <div className={ classNames 'notification', 'hidden': @state.notifications.subscribers is 0 }>
              {
                if @state.notifications.subscribers <= 99
                  <p>{ if @state.notifications.subscribers > 0 then @state.notifications.subscribers }</p>
                else
                  <p>99+</p>
              }
            </div>
          </div>
        </div>
      </div>
    </div>



export default outfit AdminMenu
