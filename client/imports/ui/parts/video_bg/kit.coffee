import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'



VideoBg = class extends Component
  constructor: (props) ->
    super props



  componentDidMount: =>
    @initVideo()



  initVideo: =>
    selector = $('#header-filter-layout')

    video_id = 'XeNUOFZtVMM'

    player = {}

    startSeconds = 0
    endSeconds = 9.5
    done = no

    loadPlayer = =>
      if typeof(YT) is 'undefined' or typeof(YT.Player) is 'undefined'
        tag = document.createElement 'script'
        tag.src = "https://www.youtube.com/iframe_api"
        firstScriptTag = document.getElementsByTagName('script')[0]
        firstScriptTag.parentNode.insertBefore tag, firstScriptTag

        window.onYouTubePlayerAPIReady = =>
          onYouTubePlayer()

      else
        onYouTubePlayer()

    onYouTubePlayer = =>
      width = =>
        if $(window).width() < 1440
          1440
        else
          $(window).width() + 160

      data =
        width: width()
        height: width() / 1.6
        videoId: video_id
        playerVars:
          controls: 0
          start: startSeconds
        events:
          'onReady': onPlayerReady
          'onStateChange': onPlayerStateChange

      player = new YT.Player 'YTPlayer', data

    onPlayerReady = =>
      player.hideVideoInfo()
      player.setPlaybackQuality 'hd720'
      player.mute()
      player.playVideo()
      player.setPlaybackRate 0.9

    onPlayerStateChange = (event) =>
      if event.data is YT.PlayerState.PLAYING and not done
        setInterval =>
          if player.getCurrentTime() >= endSeconds
            player.seekTo startSeconds
        , 200
        selector.animate
          opacity: 1
        , 1000
        done = yes

    loadPlayer()



  render: =>
    if not @props.app.user.logged
     style = { position: 'fixed', pointerEvents: 'none', top: 0, left: '0', width: '100vw', height: '110vh', overflow: 'hidden' }
    else
     style = { position: 'fixed', pointerEvents: 'none', top: 0, left: '70vw', width: '100vw', height: '110vh', overflow: 'hidden' }
    <div className="header-filter" style={ style }>
      <div style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', background: "url('/img/pictures/video_bg.jpg') center/cover no-repeat" }}></div>
      <div id='header-filter-layout' style={{ position: 'relative', height: 0, top: '-120px', paddingBottom: 'calc(110vh + 120px)', opacity: 0, overflow: 'hidden' }}>
        <div id='iframe-player' style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' }}>
          <div id='YTPlayer'></div>
        </div>
      </div>
      <div style={{ position: 'absolute', width: '100%', height: '100%', top: 0, left: 0, background: 'rgba(0,0,0, 0.2)' }}></div>
    </div>



export default outfit VideoBg
