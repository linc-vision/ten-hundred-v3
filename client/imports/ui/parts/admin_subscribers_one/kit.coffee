import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'

import './kit.styl'

import { AdminRequests } from '../kit'



AdminSubscribersOne = class extends Component
  constructor: (props) ->
    super props
    @state =
      user: _.findWhere(@props.data.users, { _id: @props.match.params.user }) or null



  componentDidMount: =>
    @props.app.changePage 'subscribers'

    $('#App > #layout').animate
      scrollTop: 0

  componentDidUpdate: (prevProps) =>
    if @props.data.users
      if not _.isEqual _.findWhere(@props.data.users, { _id: @props.match.params.user }), @state.user
        @setState
          user: _.findWhere(@props.data.users, { _id: @props.match.params.user })



  back: =>
    @props.history.push '/subscribers'



  render: =>
    <div id='AdminSubscribersOne'>
      {
        if @state.user
          if @state.user.profile.credit_cards.length > 0
            default_card = _.filter(@state.user.profile.credit_cards, (card) => card.stripe.id is @state.user.profile.customer_data.default_source)[0]
            default_card.card_number = default_card.card_number.replace(/[^0-9]+/g, '')
          <div id='layout'>
            <div id='header'>
              <h1>{ @state.user.emails[0].address }</h1>
              {
                if @state.user.profile.phone_number.length > 0
                  <h2>{ @state.user.profile.phone_number }</h2>
              }
              <button className='button_grey' onClick={ @back }>Back</button>
            </div>
            {
              if @state.user.profile.credit_cards.length > 0 and @state.user.profile.membership.status is 'active'
                <div id='information'>
                  <div id='card'>
                    <div id='icon'>
                      {
                        if default_card.stripe.brand is 'Visa'
                          <img src='/img/icons/visa.png' alt={ default_card.stripe.brand }/>
                        else if default_card.stripe.brand is 'MasterCard'
                          <img src='/img/icons/mastercard.png' alt={ default_card.stripe.brand }/>
                        else if default_card.stripe.brand is 'American Express'
                          <img src='/img/icons/american_express.png' alt={ default_card.stripe.brand }/>
                        else if default_card.stripe.brand is 'Diners Club'
                          <img src='/img/icons/diners_club.png' alt={ default_card.stripe.brand }/>
                        else if default_card.stripe.brand is 'Discover'
                          <img src='/img/icons/discover.png' alt={ default_card.stripe.brand }/>
                        else
                          <img src='/img/icons/mastercard.png' alt={ default_card.stripe.brand }/>
                      }
                    </div>
                    <div id='number'>
                      <h1>{ default_card.card_number.slice 0, 4 }</h1>
                      <h1>{ default_card.card_number.slice 4, 8 }</h1>
                      <h1>{ default_card.card_number.slice 8, 12 }</h1>
                      <h1>{ default_card.card_number.slice 12, 16 }</h1>
                    </div>
                    <div id='left'>
                      <p id='label'>Cardholder name:</p>
                      <h2 id='value'>{ default_card.name }</h2>
                    </div>
                    <div id='right'>
                      <p id='label'>Expires</p>
                      <h2 id='value'>{ "#{ default_card.stripe.exp_month }/#{ default_card.stripe.exp_year }" }</h2>
                    </div>
                    <div id='bleft'>
                      <p id='label'>Zipcode</p>
                      <h2 id='value'>{ default_card.postal_code }</h2>
                    </div>
                    <div id='bright'>
                      <p id='label'>CVV:</p>
                      <h2 id='value'>{ default_card.cvv }</h2>
                    </div>
                  </div>
                  <div id='dates'>
                    <div id='next_payment' className='stats'>
                      <h2 id='label'>Next payment:</h2>
                      <h1 id='data'>{ moment(@state.user.profile.membership.current_period_end * 1000).format 'DD/MM/YYYY' }</h1>
                    </div>
                    <div id='last_payment' className='stats'>
                      <h2 id='label'>Last payment:</h2>
                      <h1 id='data'>{ moment(@state.user.profile.membership.current_period_start * 1000).format 'DD/MM/YYYY' }</h1>
                    </div>
                    <div id='registration_date' className='stats'>
                      <h2 id='label'>Registration date:</h2>
                      <h1 id='data'>{ moment(@state.user.createdAt).format 'DD/MM/YYYY' }</h1>
                    </div>
                  </div>
                </div>
            }
            <div id='requests'>
              <AdminRequests requests={ if @state.user then _.filter(@props.data.requests, (item) => item.owner.id is @state.user._id) }/>
            </div>
          </div>
      }
    </div>



export default outfit AdminSubscribersOne
