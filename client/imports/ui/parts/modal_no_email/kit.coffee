import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



ModalNoEmail = class extends Component
  constructor: (props) ->
    super props
    @state =
      email: ''
      email_valid: null
      response:
        status: ''
        message: ''



  typeEmail: (e) =>
    @setState
      email: e.target.value

  validateEmail: =>
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    @setState
      email_valid: emailReg.test @state.email

  keyPress: (e) =>
    if e.key is 'Enter'
      @sendEmail()

  submitEmail: =>
    @validateEmail()
    setTimeout =>
      if @state.email_valid
        @setState
          response:
            status: 'loading'
            message: ''
        , =>
          Meteor.call 'checkEmail', @state.email, (err, res) =>
            if err or res.status is 'F'
              if err
                @setState
                  response:
                    status: 'ready'
                    message: 'Internal Server Error. Please try again later.'
              else
                @hideResponse()
                @setState
                  response:
                    status: 'ready'
                    message: 'This email is already in use. Please enter another one.'
                setTimeout () =>
                  @setState
                    response:
                      status: ''
                      message: ''
                , 6000
            else
              Meteor.call 'addEmail', @state.email, @props.app.user.fb_session_userId, (err, res) =>
                if err
                  @setState
                    response:
                      status: 'ready'
                      message: 'Error. Please try again later.'
                else
                  analytics.track 'A user entered an email address'
                  @hideResponse()
                  @props.closeModal()
                  @loginWithFacebook()
    , 100

  loginWithFacebook: =>
    Meteor.loginWithFacebook { requestPermissions: ['email'] }, (err, res) =>
      if err
        if err.errorType isnt 'Accounts.LoginCancelledError'
          if err.error is 403 then err.message = 'Email already exists'
          if err.error is 405
            @hideResponse()
            @props.app.setFBSession err.reason
            @props.app.openModal 'no_email'
          else
            @setState
              response:
                status: 'ready'
                message: err.message
      else
        @hideResponse()
        Animations.global.changePage =>
          @props.app.signIn()
          analytics.track "A new user logged in via Facebook"
          gtag 'event', 'conversion', { 'send_to': 'AW-755287007/svJ9CM7t9JgBEN-Hk-gC' }
          @props.history.push '/new-request'

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''



  render: =>
    <div id='ModalNoEmail' className={ if IS.mobile() then 'mobile' }>
      <div id='layout'>
        <h2 className={ classNames 'transparent': @state.response.status isnt '' }>To procees, please let us know your preferred email:</h2>
        <div id='input' className={ classNames 'transparent': @state.response.status isnt '' }>
          <input className={ classNames 'left_form', 'invalid': @state.email_valid is no } type='text' name='email' placeholder='Enter your email here...' value={ @state.email } onChange={ @typeEmail } onBlur={ @validateEmail } onKeyPress={ @keyPress }/>
          <p id='error' className={ classNames 'hidden': @state.email_valid isnt no }>Please enter your email to continue</p>
        </div>
        <button className={ classNames 'button_blue', 'transparent': @state.response.status isnt '' } onClick={ @submitEmail }>Submit</button>
        {
          if @state.response.status isnt ''
            <div id='response' onClick={ if @state.response.status is 'ready' then @props.closeModal() }>
              {
                if @state.response.status is 'ready'
                  <p>{ @state.response.message }</p>
                else
                  <div id='loading'></div>
              }
            </div>
        }
      </div>
    </div>



export default outfit ModalNoEmail
