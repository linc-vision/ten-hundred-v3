import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import { ModalCruise, ModalAlert, ModalAlertUpgrade, ModalAdminRequest, ModalAddPaymentMethod, ModalMobileMenu, ModalMembership, ModalNoEmail } from '../kit'

import classNames from 'classnames'

import Animations from '/client/imports/ui/animations/kit'

import $ from 'jquery'

import './kit.styl'



Modal = class extends Component
  constructor: (props) ->
    super props



  componentDidMount: =>
    @showBoard()



  showBoard: =>
    Animations.global.showModal()
    if IS.mobile()
      $('#App > #layout').animate
        scrollTop: 0
      , 300
      .css
        overflowY: 'hidden'
    else
      $('#App > #layout').css
        overflowY: 'hidden'


  closeBoard: =>
    if @props.app.modal.type is 'cruise'
      @props.history.push '/'
    Animations.global.hideModal =>
      @props.app.closeModal()
      $('#App > #layout').css
        overflowY: 'auto'



  render: =>
    <div id='Modal' className={ classNames 'mobile': IS.mobile(), 'no_email': @props.app.modal.type is 'no_email' }>
      <div id='layer' onClick={ @closeBoard }></div>
      <div id='board' className={ @props.app.modal.type }>
        {
          if @props.app.modal.type isnt 'alert' and @props.app.modal.type isnt 'no_email' and @props.app.modal.type isnt 'alert_upgrade'
            <div id='close' className='cursor_pointer' onClick={ @closeBoard }></div>
          else if @props.app.modal.type is 'no_email' and IS.mobile()
            <div id='close' className='cursor_pointer' onClick={ @closeBoard }></div>
        }
        {
          if @props.app.modal.type is 'cruise'
            <ModalCruise/>
          else if @props.app.modal.type is 'membership'
            <ModalMembership closeModal={ @closeBoard }/>
          else if @props.app.modal.type is 'alert'
            <ModalAlert closeModal={ @closeBoard }/>
          else if @props.app.modal.type is 'no_email'
            <ModalNoEmail closeModal={ @closeBoard }/>
          else if @props.app.modal.type is 'alert_upgrade'
            <ModalAlertUpgrade closeModal={ @closeBoard }/>
          else if @props.app.modal.type is 'admin_request'
            <ModalAdminRequest closeModal={ @closeBoard }/>
          else if @props.app.modal.type is 'add_payment_method' or @props.app.modal.type is 'add_cashback_method'
            <ModalAddPaymentMethod closeModal={ @closeBoard } method={ @props.app.modal.type }/>
          else if @props.app.modal.type is 'mobile_menu'
            <ModalMobileMenu closeModal={ @closeBoard }/>
        }
      </div>
    </div>



export default outfit Modal
