import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import { DashboardMenu, AdminMenu, EmailForm, RegisterForm, CreatePasswordForm, Back, SignIn } from '../kit'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



Right = class extends Component
  constructor: (props) ->
    super props



  openPage: (page) =>
    Animations.global.changePage =>
      if page is 'terms_of_service'
        @props.history.push '/terms-of-service'
      else if page is 'privacy_policy'
        @props.history.push '/privacy-policy'
      else if page is 'register'
        @props.history.push '/register'

  logout: =>
    Animations.global.changePage =>
      @props.app.logOut()
      @props.history.push '/home'



  render: =>
    <div id='Right' className={ classNames 'dashboard': @props.app.user.logged, 'admin': Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP }>
      {
        if @props.app.user.logged
          if not Roles.userIsInRole Meteor.userId(), ['admin'], Roles.GLOBAL_GROUP
            <div id='layout'>
              <div id='header'>
                <h2 id='email'>{ @props.app.user.profile.email }</h2>
                <p id='log_out' className='cursor_pointer' onClick={ @logout }>log out</p>
              </div>
              {
                if @props.app.user.profile.membership and @props.app.user.profile.membership.status is 'active'
                  <Switch>
                    <Route path='/new-request' render={ => <DashboardMenu/> }/>
                    <Route path='/all-requests' render={ => <DashboardMenu/> }/>
                    <Route path='/my-membership' render={ => <DashboardMenu/> }/>
                    <Route path='/terms-of-service' render={ => <Back/> }/>
                    <Route path='/privacy-policy' render={ => <Back/> }/>
                    <Route path='/:request' render={ => <DashboardMenu/> }/>
                    <Redirect from='*' to='/new-request'/>
                  </Switch>
                else
                  if @props.app.user.profile.email is '---'
                    <Switch>
                      <Route path='/my-membership' render={ => <DashboardMenu/> }/>
                      <Route path='/terms-of-service' render={ => <Back/> }/>
                      <Route path='/privacy-policy' render={ => <Back/> }/>
                      <Redirect from='*' to='/my-membership'/>
                    </Switch>
                  else
                    <Switch>
                      <Route path='/new-request' render={ => <DashboardMenu/> }/>
                      <Route path='/all-requests' render={ => <DashboardMenu/> }/>
                      <Route path='/my-membership' render={ => <DashboardMenu/> }/>
                      <Route path='/terms-of-service' render={ => <Back/> }/>
                      <Route path='/privacy-policy' render={ => <Back/> }/>
                      <Redirect from='*' to='/my-membership'/>
                    </Switch>
              }
              <div id='footer'>
                <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'terms_of_service' } onClick={ if @props.app.page.id isnt 'terms_of_service' then @openPage.bind this, 'terms_of_service' }>Terms of Service</p>
                <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'privacy_policy' } onClick={ if @props.app.page.id isnt 'privacy_policy' then @openPage.bind this, 'privacy_policy' }>Privacy Policy</p>
                <p id='copyright'>{ "©#{ new Date().getFullYear() } Ten Hundred" }</p>
              </div>
            </div>
          else
            <div id='layout'>
              <div id='header'>
                <div id='logo'>
                  <img src='/img/pictures/logo_white.png' alt='TehHundred logo'/>
                </div>
                <h2>Admin</h2>
              </div>
              <Switch>
                <Route path='/requests' render={ => <AdminMenu/> }/>
                <Route path='/subscribers' render={ => <AdminMenu/> }/>
                <Redirect from='*' to='/requests'/>
              </Switch>
              <div id='footer'>
                <p id='log_out' className='cursor_pointer' onClick={ @logout }>log out</p>
              </div>
            </div>
        else
          <div id='layout'>
            <Switch>
              <Route exact path='/' render={ => <EmailForm/> }/>
              <Route path='/cruises' render={ => <EmailForm/> }/>
              <Route path='/ski' render={ => <EmailForm/> }/>
              <Route path='/terms-of-service' render={ => <Back/> }/>
              <Route path='/privacy-policy' render={ => <Back/> }/>
              <Route path='/sign-in' render={ => <SignIn/> }/>
              <Redirect from='*' to='/'/>
            </Switch>
            {
              if @props.app.page.id isnt 'register' and @props.app.page.id isnt 'create_password'
                <div id='footer'>
                  <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'terms_of_service' } onClick={ if @props.app.page.id isnt 'terms_of_service' then @openPage.bind this, 'terms_of_service' }>Terms of Service</p>
                  <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'privacy_policy' } onClick={ if @props.app.page.id isnt 'privacy_policy' then @openPage.bind this, 'privacy_policy' }>Privacy Policy</p>
                </div>
            }
          </div>
          <div id='layout'>

          </div>
      }
    </div>



export default outfit Right
