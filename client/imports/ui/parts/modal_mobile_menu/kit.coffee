import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import { DashboardMenu } from '../kit'

import classNames from 'classnames'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



ModalMobileMenu = class extends Component
  constructor: (props) ->
    super props



  openPage: (page) =>
    @props.closeModal()
    Animations.global.changePage =>
      if page is 'privacy_policy'
        @props.history.push '/privacy-policy'
      else if page is 'terms_of_service'
        @props.history.push '/terms-of-service'
      else if page is 'sign_in'
        @props.history.push '/sign-in'
      else
        @props.history.push '/home'



  logout: =>
    @props.closeModal()
    Animations.global.changePage =>
      @props.app.logOut()
      @props.history.push '/home'



  render: =>
    <div id='ModalMobileMenu'>
      {
        if @props.app.user.logged
          <div id='layout' className='scrollbar_hidden'>
            <div id='header'>
              <h2 id='title' className='email'>{ @props.app.user.profile.email }</h2>
            </div>
            <div id='content'>
              <div id='menu'>
                <DashboardMenu closeModal={ @props.closeModal }/>
                <h2 id='log_out' className='cursor_pointer' onClick={ @logout }>log out</h2>
              </div>
            </div>
            <div id='footer'>
              <p className={ classNames 'active': @props.app.page.id is 'terms_of_service' } onClick={ @openPage.bind this, 'terms_of_service' }>Terms of Service</p>
              <p className={ classNames 'active': @props.app.page.id is 'privacy_policy' } onClick={ @openPage.bind this, 'privacy_policy' }>Privacy Policy</p>
              <p>{ "©#{ new Date().getFullYear() } Ten Hundred" }</p>
            </div>
          </div>
        else
          <div id='layout'>
            <div id='header'>
              <h2 id='title'>Menu</h2>
            </div>
            <div id='content' className={ classNames 'not_logged': not @props.app.user.logged }>
              <div id='menu'>
                <h2 className={ classNames 'active': @props.app.page.id is 'sign_in' } onClick={ if @props.app.page.id isnt 'sign_in' then @openPage.bind this, 'sign_in' }>Sign in</h2>
                <h2 className={ classNames 'active': @props.app.page.id is 'terms_of_service' } onClick={ if @props.app.page.id isnt 'terms_of_service' then @openPage.bind this, 'terms_of_service' }>Terms of Service</h2>
                <h2 className={ classNames 'active': @props.app.page.id is 'privacy_policy' } onClick={ if @props.app.page.id isnt 'privacy_policy' then @openPage.bind this, 'privacy_policy' }>Privacy Policy</h2>
              </div>
            </div>
            <div id='footer' className={ classNames 'not_logged': not @props.app.user.logged }>
              <p>{ "©#{ new Date().getFullYear() } Ten Hundred" }</p>
            </div>
          </div>
      }
    </div>



export default outfit ModalMobileMenu
