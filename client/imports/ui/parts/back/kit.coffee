import React, { Component } from 'react'

import Animations from '/client/imports/ui/animations/kit'

import { outfit } from '/client/imports/redux/outfit'

import './kit.styl'



Back = class extends Component
  constructor: (props) ->
    super props
    @state =
      email: ''
      email_sent: no



  openPage: (page) =>
    Animations.global.changePage =>
      if page is 'register'
        @props.history.push '/home'
        @props.app.changePage 'register'
      else if page is 'sign_in'
        @props.history.push '/sign-in'
      else
        @props.history.push '/home'



  render: =>
    <div id='Back'>
      <div id='layout'>
        <div id='form'>
          <button className='button_blue' onClick={ @openPage.bind this, @props.app.page.previous_id }>Back</button>
        </div>
      </div>
    </div>



export default outfit Back
