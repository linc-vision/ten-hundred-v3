import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import './kit.styl'



ModalCruise = class extends Component
  constructor: (props) ->
    super props
    @state =
      cruises: @props.data.cruises



  componentDidMount: =>
    @props.app.changePage 'cruises'
    analytics.track 'Viewed Cruises on the landing page'



  render: =>
    <div id='ModalCruise' className={ if IS.mobile() then 'mobile' }>
      <div id='layout'>
        <div id='header'>
          <div id='icon'></div>
          <h2 id='title'>Ocean & River<br/>Cruises</h2>
        </div>
        <div id='list' className='scrollbar_hidden'>
          <div id='layout'>
            {
              @state.cruises.map (element, n) =>
                <div key={ n } className='item'>
                  <p id='title'>{ element.title }</p>
                  <span></span>
                  <p id='percentage'>{ element.percentage }</p>
                </div>
            }
          </div>
        </div>
      </div>
    </div>



export default outfit ModalCruise
