import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'

import './kit.styl'



AdminSubscribers = class extends Component
  constructor: (props) ->
    super props
    @state =
      users: @props.data.users or []
      search_query: ''
      gold_count: _.filter(@props.data.users, (item) -> item.profile.membership.plan.nickname is 'gold').length
      silver_count: _.filter(@props.data.users, (item) -> item.profile.membership.plan.nickname is 'silver').length



  componentDidMount: =>
    @props.app.changePage 'subscribers'

    $('#App > #layout').animate
      scrollTop: 0

  componentDidUpdate: (prevProps) =>
    if @props.data.users
      if not _.isEqual @props.data.users, @state.users
        @setState
          users: @props.data.users
        , =>
          @setState
            gold_count: _.filter(@state.users, (item) -> item.profile.membership.plan.nickname is 'gold').length
            silver_count: _.filter(@state.users, (item) -> item.profile.membership.plan.nickname is 'silver').length



  typeSearchQuery: (e) =>
    @setState
      search_query: e.target.value

  openSubscriber: (id) =>
    @props.history.push "/subscribers/#{ id }"



  render: =>
    <div id='AdminSubscribers'>
      <div id='layout'>
        <div id='header'>
          <div id='gold' className='stats'>
            <h1 id='data'>{ @state.gold_count }</h1>
            <div id='icon'></div>
          </div>
          <div id='silver' className='stats'>
            <h1 id='data'>{ @state.silver_count }</h1>
            <div id='icon'></div>
          </div>
          <div id='total' className='stats'>
            <h1 id='data'>{ @state.users.length }</h1>
            <h2 id='label'>Total<br/>subscribers</h2>
          </div>
          <div id='emails' className='stats'>
            <h1 id='data'>{ @props.data.emails.length }</h1>
            <h2 id='label'>Email<br/>addresses</h2>
          </div>
          <div id='income' className='stats'>
            <h1 id='data'>{ "$#{ @state.gold_count * 149 + @state.silver_count * 99 }" }</h1>
            <h2 id='label'>Income<br/>per year</h2>
          </div>
        </div>
        <div id='search'>
          <input className='left_form' type='text' name='search' placeholder='Search by email address...' value={ @state.search_query } onChange={ @typeSearchQuery }/>
        </div>
        <div id='list'>
          {
            if @state.users.length > 0
              list = _.filter(@state.users, (item) => item.emails[0].address.includes @state.search_query)
              if list.length > 0
                list.map (user, n) =>
                  if not user.viewedByAdmin
                    Meteor.call 'viewUser', user._id
                  if user.emails[0].address.includes @state.search_query
                    <div key={ n } className={ classNames 'user', user.profile.membership.plan.nickname }>
                      <h2 className='cursor_pointer' onClick={ @openSubscriber.bind this, user._id }>{ user.emails[0].address }</h2>
                    </div>
              else
                <div id='empty'>
                  <h2>Subscribers not found</h2>
                </div>
            else
              <div id='empty'>
                <h2>There are no subscribers yet</h2>
              </div>
          }
        </div>
        {
          if @props.data.emails.length > 0
            <div id='emails_list'>
              <h2>Not members:</h2>
              {
                  @props.data.emails.map (email, n) =>
                    <div key={ n } className='email'>
                      <h2 className='selectable_text'>{ email.email }</h2>
                    </div>
              }
            </div>
        }
      </div>
    </div>



export default outfit AdminSubscribers
