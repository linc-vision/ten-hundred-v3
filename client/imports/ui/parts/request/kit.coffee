import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import $ from 'jquery'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



Request = class extends Component
  constructor: (props) ->
    super props
    @state =
      request: _.findWhere(@props.data.requests, { _id: @props.match.params.request }) or null
      selected_payment_method: _.findWhere(@props.app.user.profile.credit_cards, { id: @props.app.user.profile.customer_data.default_source }) or {}
      selected_cashback_method: _.first(@props.app.user.profile.bank_accounts) or _.first(@props.app.user.profile.addresses) or null
      selected_cashback_methodValid: null



  componentDidMount: =>
    @props.app.changePage 'request'

    $('#App > #layout').animate
      scrollTop: 0

  componentDidUpdate: (prevProps) =>
    if @props.data.requests
      if not _.isEqual _.findWhere(@props.data.requests, { _id: @props.match.params.request }), @state.request
        request = _.findWhere(@props.data.requests, { _id: @props.match.params.request })
        if request
          if request.hasOwnProperty 'confirmation'
            if request.status is 'confirmation'
              @setState
                request: request
            else
              @props.history.push '/all-requests'
          else
            @props.history.push '/all-requests'
        else
          @props.history.push '/all-requests'

    if @state.selected_cashback_method is null
      prevMethods = []
      methods = []

      if prevProps.app.user.profile.bank_accounts
        methods = _.union methods, prevProps.app.user.profile.bank_accounts
      if prevProps.app.user.profile.addresses
        methods = _.union methods, prevProps.app.user.profile.addresses

      if @props.app.user.profile.bank_accounts
        methods = _.union methods, @props.app.user.profile.bank_accounts
      if @props.app.user.profile.addresses
        methods = _.union methods, @props.app.user.profile.addresses

      if not _.isEqual prevMethods, methods
        @setState
          selected_cashback_method: _.first methods



  selectPaymentMethod: (method) =>
    @setState
      selected_payment_method: method or @state.selected_payment_method

  selectCashbackMethod: (type, method) =>
    checkIsValid = =>
      if @state.selected_cashback_method
        @setState
          selected_cashback_methodValid: yes

    if type is 'account'
      @setState
        selected_cashback_method: method or @state.selected_cashback_method
      , => checkIsValid()
    else if type is 'address'
      @setState
        selected_cashback_method: method or @state.selected_cashback_method
      , => checkIsValid()

  confirmDetails: =>
    if not @state.selected_cashback_method
      @setState
        selected_cashback_methodValid: no
    else
      @setState
        selected_cashback_methodValid: yes
      Meteor.call 'confirmRequest', @state.request._id, @state.selected_payment_method, @state.selected_cashback_method, (err, res) =>
        if not err
          if res.status is 'S'
            analytics.track 'Subscriber confirmed the details'
            @props.history.push '/all-requests'



  render: =>
    <div id='Request' className={ classNames 'mobile': IS.mobile() }>
      {
        if @state.request
          <div id='layout'>
            <div id='header'>
              <div id='logo'>
                <img src='/img/pictures/logo.png' alt='Ten Hundred Logo'/>
              </div>
              {
                if IS.mobile()
                  <div id='menu_button' onClick={ @props.app.openModal.bind this, 'mobile_menu' }>
                    <div id='icon'></div>
                  </div>
              }
              <div id='total_saving'>
                <h2 id='label'>Total savings:</h2>
                <h2 id='amount'>{ "$#{ @props.app.user.profile.total_savings }" }</h2>
              </div>
            </div>
            <div id='content'>
              <div id='icon' className={ @state.request.category }></div>
              <h1 id='title'>Confirmation:</h1>
              <div id='details'>
                <h2 id='price'>Price: <span>{ "$#{ @state.request.confirmation.price }" }</span></h2>
                <div id='supplier'>
                  <h2 id='label'>Supplier:</h2>
                  <h2 id='data'>{ @state.request.supplier.title }</h2>
                </div>
                {
                  if @state.request.confirmation.comment.length > 0
                    <div id='comment'>
                      <h2 id='label'>{ if @state.request.status is 'confirmed' or @state.request.status is 'completed' then 'Confirmation comment:' else 'Comment:' }</h2>
                      <p id='data'>{ @state.request.confirmation.comment }</p>
                    </div>
                }
                {
                  if @state.request.status is 'confirmed' or @state.request.status is 'completed'
                    if @state.request.comment.length > 0
                      <div id='comment'>
                        <h2 id='label'>Your comment:</h2>
                        <p id='data'>{ @state.request.comment }</p>
                      </div>
                }
              </div>
              <div id='right'>
                <div id='cashback'>
                  <h2>CASH BACK: <span>{ "you earned US$#{ @state.request.confirmation.cashback }" }</span></h2>
                  <p>{ "Estimated #{ @state.request.confirmation.date }" }</p>
                </div>
                <div id='badge'></div>
              </div>
              <div id='payment_method'>
                <h2 id='label'>Payment method:</h2>
                {
                  if @state.request.status is 'confirmation'
                    <div id='list'>
                      {
                        if @props.app.user.profile.credit_cards
                          @props.app.user.profile.credit_cards.map (method, n) =>
                            <div key={ n } className={ classNames 'element', 'cursor_pointer': @state.selected_payment_method.id isnt method.id, 'active': @state.selected_payment_method.id is method.id } onClick={ @selectPaymentMethod.bind this, method }>
                              <h2>****</h2>
                              <h2>****</h2>
                              <h2>****</h2>
                              <h2>{ method.last4 }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      <div id='add_payment_method' className='cursor_pointer' onClick={ @props.app.openModal.bind this, 'add_payment_method' }>
                        <p>+</p>
                      </div>
                    </div>
                  else
                    <div id='list'>
                      <div className='element active'>
                        <h2>****</h2>
                        <h2>****</h2>
                        <h2>****</h2>
                        <h2>{ @state.request.confirmation.payment_method.last4 }</h2>
                        <div id='select'></div>
                      </div>
                    </div>
                }
              </div>
              <div id='cashback_method'>
                <h2 id='label' className={ classNames 'invalid': @state.selected_cashback_methodValid is no }>Cashback method:</h2>
                {
                  if @state.request.status is 'confirmation'
                    <div id='list'>
                      {
                        find = _.first(@props.app.user.profile.bank_accounts) or null
                        if find
                          @props.app.user.profile.bank_accounts.map (method, n) =>
                            <div key={ n } className={ classNames 'element', 'cursor_pointer': @state.selected_cashback_method and @state.selected_cashback_method.id isnt method.id, 'active': @state.selected_cashback_method and @state.selected_cashback_method.id is method.id } onClick={ @selectCashbackMethod.bind this, 'account', method }>
                              <div id='icon' className='account'></div>
                              <p className='account'>Bank</p>
                              <h2 id='account'>{ "************#{ method.account.slice(method.account.length - 4, method.account.length) }" }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      {
                        find = _.first(@props.app.user.profile.addresses) or null
                        if find
                          @props.app.user.profile.addresses.map (method, n) =>
                            <div key={ if @props.app.user.profile.bank_accounts then @props.app.user.profile.bank_accounts.length + n else n } className={ classNames 'element', 'cursor_pointer': @state.selected_cashback_method and @state.selected_cashback_method.id isnt method.id, 'active': @state.selected_cashback_method and @state.selected_cashback_method.id is method.id } onClick={ @selectCashbackMethod.bind this, 'address', method }>
                              <div id='icon' className='address'></div>
                              <p className='address'>Check</p>
                              <h2 id='address'>{ "#{ (if method.original[0].formatted_address then method.original[0].formatted_address else method.original[0].name).slice(0, 16) }..." }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      <div id='add_payment_method' className='cursor_pointer' onClick={ @props.app.openModal.bind this, 'add_cashback_method' }>
                        <p>+</p>
                      </div>
                      <p id='error' className={ classNames 'hidden': @state.selected_cashback_methodValid isnt no }>Please select your cashback method before continuing.</p>
                    </div>
                  else
                    if @state.request.confirmation.cashback_method.object is 'account'
                      <div id='list'>
                        <div className='element active'>
                          <div id='icon' className='account'></div>
                          <p className='account'>Bank</p>
                          <h2 id='account'>{ "************#{ @state.request.confirmation.cashback_method.account.slice(@state.request.confirmation.cashback_method.account.length - 4, @state.request.confirmation.cashback_method.account.length) }" }</h2>
                          <div id='select'></div>
                        </div>
                      </div>
                    else if @state.request.confirmation.cashback_method.object is 'address'
                      <div id='list'>
                        <div className='element active'>
                          <div id='icon' className='account'></div>
                          <p className='address'>Check</p>
                          <h2 id='address'>{ "#{ (if @state.request.confirmation.cashback_method.original[0].formatted_address then @state.request.confirmation.cashback_method.original[0].formatted_address else @state.request.confirmation.cashback_method.original[0].name).slice(0, 20) }..." }</h2>
                          <div id='select'></div>
                        </div>
                      </div>
                }
              </div>
              {
                if @state.request.status is 'confirmation'
                  <button className='button_blue' onClick={ @confirmDetails }>Confirm Details</button>
              }
            </div>
          </div>
      }
    </div>



export default outfit Request
