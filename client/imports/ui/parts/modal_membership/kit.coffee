import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'
import anime from 'animejs'

import { RegisterForm } from '../kit'

import './kit.styl'



ModalMembership = class extends Component
  constructor: (props) ->
    super props
    @state =
      page: 'offer'
      request: @props.app.modal.data



  continue: =>
    analytics.track 'A user clicked "Join Membership" button'
    anime
      targets: ['#ModalMembership > #layout > #content, #ModalMembership > #offer, #ModalMembership > #layer']
      duration: 400
      easing: 'easeInOutExpo'
      scale: [1, 0.6]
      opacity: [1, 0]
      complete: =>
        @setState
          page: 'form'
        , =>
          anime
            targets: ['#ModalMembership > #layout > #content']
            duration: 400
            easing: 'easeInOutExpo'
            scale: [0.6, 1]
            opacity: [0, 1]



  render: =>
    <div id='ModalMembership' className={ if IS.mobile() then 'mobile' }>
      {
        if @state.page is 'offer'
          <div id='offer'>
            <h3>You will first need to become a TenHundred member in order to continue with your purchase.</h3>
            <h2>Ready for cash back?</h2>
            <button className='button_blue' onClick={ @continue }>Join Membership</button>
          </div>
      }
      <div id='layout'>
        <div id='content'>
          {
            if @state.page is 'offer'
              <div id='response'>
                <div id='content'>
                  <div id='icon' className={ @state.request.category }></div>
                  <h1 id='title'>Confirmation:</h1>
                  <div id='details'>
                    <h2 id='price'>Price: <span>{ "$#{ @state.request.confirmation.price }" }</span></h2>
                    <div id='supplier'>
                      <h2 id='label'>Supplier:</h2>
                      <h2 id='data'>{ @state.request.supplier.title }</h2>
                    </div>
                    {
                      if @state.request.confirmation.comment.length > 0
                        <div id='comment'>
                          <h2 id='label'>{ if @state.request.status is 'confirmed' or @state.request.status is 'completed' then 'Confirmation comment:' else 'Comment:' }</h2>
                          <p id='data'>{ @state.request.confirmation.comment }</p>
                        </div>
                    }
                  </div>
                  <div id='right'>
                    <div id='cashback'>
                      <h2>CASH BACK: <span>{ "you earned US$#{ @state.request.confirmation.cashback }" }</span></h2>
                      <p>{ "Estimated #{ @state.request.confirmation.date }" }</p>
                    </div>
                    <div id='badge'></div>
                  </div>
                  <div id='payment_method'>
                    <h2 id='label'>Payment method:</h2>
                    <div id='list'>
                      {
                        if @props.app.user.profile.credit_cards
                          @props.app.user.profile.credit_cards.map (method, n) =>
                            <div key={ n } className={ classNames 'element', 'cursor_pointer': @state.selected_payment_method.id isnt method.id, 'active': @state.selected_payment_method.id is method.id } onClick={ @selectPaymentMethod.bind this, method }>
                              <h2>****</h2>
                              <h2>****</h2>
                              <h2>****</h2>
                              <h2>{ method.last4 }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      <div id='add_payment_method' className='cursor_pointer' onClick={ @props.app.openModal.bind this, 'add_payment_method' }>
                        <p>+</p>
                      </div>
                    </div>
                  </div>
                  <div id='cashback_method'>
                    <h2 id='label' className={ classNames 'invalid': @state.selected_cashback_methodValid is no }>Cashback method:</h2>
                    <div id='list'>
                      {
                        find = _.first(@props.app.user.profile.bank_accounts) or null
                        if find
                          @props.app.user.profile.bank_accounts.map (method, n) =>
                            <div key={ n } className={ classNames 'element', 'cursor_pointer': @state.selected_cashback_method and @state.selected_cashback_method.id isnt method.id, 'active': @state.selected_cashback_method and @state.selected_cashback_method.id is method.id } onClick={ @selectCashbackMethod.bind this, 'account', method }>
                              <div id='icon' className='account'></div>
                              <p className='account'>Bank</p>
                              <h2 id='account'>{ "************#{ method.account.slice(method.account.length - 4, method.account.length) }" }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      {
                        find = _.first(@props.app.user.profile.addresses) or null
                        if find
                          @props.app.user.profile.addresses.map (method, n) =>
                            <div key={ if @props.app.user.profile.bank_accounts then @props.app.user.profile.bank_accounts.length + n else n } className={ classNames 'element', 'cursor_pointer': @state.selected_cashback_method and @state.selected_cashback_method.id isnt method.id, 'active': @state.selected_cashback_method and @state.selected_cashback_method.id is method.id } onClick={ @selectCashbackMethod.bind this, 'address', method }>
                              <div id='icon' className='address'></div>
                              <p className='address'>Check</p>
                              <h2 id='address'>{ "#{ (if method.original[0].formatted_address then method.original[0].formatted_address else method.original[0].name).slice(0, 16) }..." }</h2>
                              <div id='select'></div>
                            </div>
                      }
                      <div id='add_payment_method' className='cursor_pointer' onClick={ @props.app.openModal.bind this, 'add_cashback_method' }>
                        <p>+</p>
                      </div>
                      <p id='error' className={ classNames 'hidden': @state.selected_cashback_methodValid isnt no }>Please select your cashback method before continuing.</p>
                    </div>
                  </div>
                </div>
              </div>
            else
              <div id='form'>
                <RegisterForm closeModal={ @props.closeModal.bind this }/>
              </div>
          }
        </div>
      </div>
    </div>



export default outfit ModalMembership
