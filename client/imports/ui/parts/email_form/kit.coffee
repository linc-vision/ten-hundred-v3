import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import { VideoBg } from '../kit'

import './kit.styl'



EmailForm = class extends Component
  constructor: (props) ->
    super props
    @state =
      email: ''
      email_valid: null
      response:
        status: ''
        message: ''



  typeEmail: (e) =>
    @setState
      email: e.target.value

  validateEmail: =>
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    @setState
      email_valid: emailReg.test @state.email

  keyPress: (e) =>
    if e.key is 'Enter'
      @sendEmail()

  sendEmail: =>
    @validateEmail()
    setTimeout =>
      if @state.email_valid
        @setState
          response:
            status: 'loading'
            message: ''
        , =>
          Meteor.call 'checkEmail', @state.email, (err, res) =>
            if err or res.status is 'F'
              if err
                @setState
                  response:
                    status: 'ready'
                    message: 'Internal Server Error. Please try again later.'
              else
                @props.app.saveEmail @state.email
                @hideResponse()
                @openPage 'sign_in'
            else
              @props.app.saveEmail @state.email
              analytics.track 'A user entered an email address'
              @hideResponse()
              @openPage 'register'
    , 100

  loginWithGoogle: =>
    Meteor.loginWithGoogle { requestPermissions: ['email'] }, (err, res) =>
      if err
        if err.errorType isnt 'Accounts.LoginCancelledError'
          if err.error is 403 then err.message = 'Email already exists'
          @setState
            response:
              status: 'ready'
              message: err.message
      else
        @hideResponse()
        Animations.global.changePage =>
          @props.app.signIn()
          analytics.track "A new user logged in via Google"
          gtag 'event', 'conversion', { 'send_to': 'AW-755287007/svJ9CM7t9JgBEN-Hk-gC' }
          @props.history.push '/new-request'

  loginWithFacebook: =>
    Meteor.loginWithFacebook { requestPermissions: ['email'] }, (err, res) =>
      if err
        if err.errorType isnt 'Accounts.LoginCancelledError'
          if err.error is 403 then err.message = 'Email already exists'
          if err.error is 405
            @hideResponse()
            @props.app.setFBSession err.reason
            @props.app.openModal 'no_email'
          else
            @setState
              response:
                status: 'ready'
                message: err.message
      else
        @hideResponse()
        Animations.global.changePage =>
          @props.app.signIn()
          analytics.track "A new user logged in via Facebook"
          gtag 'event', 'conversion', { 'send_to': 'AW-755287007/svJ9CM7t9JgBEN-Hk-gC' }
          @props.history.push '/new-request'

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''

  openPage: (page) =>
    Animations.global.toggleForm =>
      if page is 'sign_in'
        @props.history.push '/sign-in'
      else if page is 'register'
        @props.app.changePage 'register'



  render: =>
    <div id='EmailForm' className={ if @props.mobile then 'mobile' }>
      {
        if @props.mobile
          <div id='video_bg'>
            <VideoBg/>
          </div>
      }
      <div id='layout'>
        <div id='form'>
          {
            if @state.response.status isnt ''
              <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                {
                  if @state.response.status is 'ready'
                    <p>{ @state.response.message }</p>
                  else
                    <div id='loading'></div>
                }
              </div>
          }
          <h1 className={ classNames 'transparent': @state.response.status isnt '' }>See how much you can save.</h1>
          <div id='input' className={ classNames 'transparent': @state.response.status isnt '' }>
            <input className={ classNames 'right_form', 'invalid': @state.email_valid is no } type='text' name='email' placeholder='Type your email here...' value={ @state.email } onChange={ @typeEmail } onBlur={ @validateEmail } onKeyPress={ @keyPress }/>
            <p id='error' className={ classNames 'hidden': @state.email_valid isnt no }>Please enter your email to continue.</p>
          </div>
          <button className={ classNames 'transparent': @state.response.status isnt '', 'button_blue' } onClick={ @sendEmail }>Continue</button>
          <p className={ classNames 'transparent': @state.response.status isnt '' }>Already a member? <span className='cursor_pointer' onClick={ @openPage.bind this, 'sign_in' }>Log in</span></p>
          <div id='socials' className={ classNames 'transparent': @state.response.status isnt '' }>
            <div id='or'>
              <div id='left' className='line'></div>
              <p>Or sign in with:</p>
                <div id='right' className='line'></div>
            </div>
            <div id='google' className={ classNames 'socials', 'cursor_pointer' } onClick={ @loginWithGoogle }>
              <div id='icon'></div>
              <p>Google</p>
            </div>
            <div id='facebook' className={ classNames 'socials', 'cursor_pointer' } onClick={ @loginWithFacebook }>
              <div id='icon'></div>
              <p>Facebook</p>
            </div>
          </div>
        </div>
      </div>
    </div>



export default outfit EmailForm
