import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import { SelectMembership } from '../kit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



RegisterForm = class extends Component
  constructor: (props) ->
    super props
    @state =
      active_plan: @props.app.register_form.active_plan
      cashback:
        cruise: @props.app.register_form.cashback.cruise
        ski: @props.app.register_form.cashback.ski
      form:
        name: @props.app.register_form.form.name
        nameValid: null
        card_number: @props.app.register_form.form.card_number
        card_numberValid: null
        cvv: @props.app.register_form.form.cvv
        cvvValid: null
        exp_date: @props.app.register_form.form.exp_date
        exp_dateValid: null
        postal_code: @props.app.register_form.form.postal_code
        postal_codeValid: null
      response:
        status: ''
        message: ''



  componentDidMount: =>
    if @props.app.user.profile.email.length is 0
      @props.history.push '/'

  componentDidUpdate: (prevProps) =>
    if @props.app.register_form.active_plan isnt @state.active_plan
      @setState
        active_plan: @props.app.register_form.active_plan



  typeName: (e) =>
    @setState
      form: {
        @state.form...
        name: e.target.value.replace(/[^A-z ]+/g, '')
      }
    , =>
      @props.app.modifyRegisterForm @state

  validateName: =>
    @setState
      form: {
        @state.form...
        nameValid: if @state.form.name.length > 0 then yes else no
      }
    , =>
      @props.app.modifyRegisterForm @state

  typeCardNumber: (e) =>
    value = e.target.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ').trim()

    @setState
      form: {
        @state.form...
        card_number: if value.replace(/[^0-9]+/g, '').length <= 16 then value else @state.form.card_number
      }
    , =>
      @props.app.modifyRegisterForm @state

  validateCardNumber: =>
    @setState
      form: {
        @state.form...
        card_numberValid: if @state.form.card_number.replace(/[^0-9]+/g, '').length >= 14 then yes else no
      }
    , =>
      @props.app.modifyRegisterForm @state

  typeCvv: (e) =>
    @setState
      form: {
        @state.form...
        cvv: if e.target.value.length <= 4 then e.target.value.replace(/[^0-9]+/g, '') else @state.form.cvv
      }
    , =>
      @props.app.modifyRegisterForm @state

  validateCvv: =>
    @setState
      form: {
        @state.form...
        cvvValid: if @state.form.cvv.length >= 3 and @state.form.cvv.length <= 4 then yes else no
      }
    , =>
      @props.app.modifyRegisterForm @state

  typeExpDate: (e) =>
    @setState
      form: {
        @state.form...
        exp_date: e.target.value.replace(/^([1-9]\/|[2-9])$/g, '0$1/').replace(/^(0[1-9]|1[0-2])$/g, '$1/').replace(/^([0-1])([3-9])$/g, '0$1/$2').replace(/^(\d)\/(\d\d)$/g, '0$1/$2').replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2').replace(/^([0]+)\/|[0]+$/g, '0').replace(/[^\d\/]|^[\/]*$/g, '').replace(/\/\//g, '/')
      }
    , =>
      @props.app.modifyRegisterForm @state

  validateExpDate: =>
    if @state.form.exp_date
      current_year = new Date().getFullYear() - 1
      selected_year = parseInt("20#{ @state.form.exp_date.split('/')[1] }")
      valid = current_year < selected_year
    else
      valid = no

    @setState
      form: {
        @state.form...
        exp_dateValid: valid
      }
    , =>
      @props.app.modifyRegisterForm @state

  typePostalCode: (e) =>
    @setState
      form: {
        @state.form...
        postal_code: e.target.value.replace(/[^0-9]+/g, '')
      }
    , =>
      @props.app.modifyRegisterForm @state

  validatePostalCode: =>
    @setState
      form: {
        @state.form...
        postal_codeValid: if @state.form.postal_code.length > 4 then yes else no
      }
    , =>
      @props.app.modifyRegisterForm @state

  validateAll: =>
    current_year = new Date().getFullYear() - 1
    selected_year = parseInt("20#{ @state.form.exp_date.split('/')[1] }")

    valid = current_year < selected_year

    @setState
      form: {
        @state.form...
        nameValid: if @state.form.name.length > 0 then yes else no
        card_numberValid: if @state.form.card_number.replace(/[^0-9]+/g, '').length >= 14 then yes else no
        cvvValid: if @state.form.cvv.length >= 3 and @state.form.cvv.length <= 4 then yes else no
        exp_dateValid: valid
        postal_codeValid: if @state.form.postal_code.length > 4 then yes else no
      }
    , =>
      @props.app.modifyRegisterForm @state

  keyPress: (e) =>
    if e.key is 'Enter'
      @submitCard()

  submitCard: =>
    @validateAll()
    setTimeout =>
      if @state.form.nameValid and @state.form.card_numberValid and @state.form.cvvValid and @state.form.exp_dateValid and @state.form.postal_codeValid
        @setState
          response:
            status: 'loading'
            message: ''
        , =>
          @props.app.modifyRegisterForm @state

          credit_card =
            name: @state.form.name
            card_number: @state.form.card_number
            cvv: @state.form.cvv
            exp_date: @state.form.exp_date
            postal_code: @state.form.postal_code

          if @props.app.register_form.promo
            plan =
              plan: @props.app.register_form.active_plan
              promo: @props.app.register_form.promo
          else
            plan = @props.app.register_form.active_plan

          Meteor.call 'subscribeCustomer', @props.app.user.profile.email, credit_card, plan, (err, res) =>
            if err or res.status is 'F'
              if res.status is 'F'
                @setState
                  response:
                    status: 'ready'
                    message: if res.message.message is 'Your card number is incorrect.' then 'Your card was declined. Please try again.' else res.message.message
                , =>
                  @props.app.modifyRegisterForm @state
                  setTimeout () =>
                    if @state.response.status isnt '' and @state.response.message isnt ''
                      @hideResponse()
                  , 8000
              else
                @setState
                  response:
                    status: 'ready'
                    message: 'Server Error. Please try again later.'
                , =>
                  @props.app.modifyRegisterForm @state
                  setTimeout () =>
                    if @state.response.status isnt '' and @state.response.message isnt ''
                      @hideResponse()
                  , 8000
            else
              if res.status is 'S'
                @props.app.setCustomer res.message.customer_data
                @props.app.setStripeCard res.message.card
                @props.app.setMembership res.message.membership
                Meteor.call 'checkSubscription', (err, res) =>
                  if not err
                    @props.app.signIn()
                  if @props.app.modal.opened
                    @props.closeModal()
    , 100

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''
      , =>
        @props.app.modifyRegisterForm @state

  openPage: (page) =>
    if @props.app.modal.opened
      @props.app.closeModal()
    Animations.global.changePage =>
      if page is 'terms_of_service'
        if @props.app.modal.opened
          @props.app.closeModal()
        @props.history.push '/terms-of-service'
      else if page is 'privacy_policy'
        @props.history.push '/privacy-policy'
      else if page is 'sign_in'
        @props.history.push '/sign-in'



  render: =>
    <div id='RegisterForm' className={ classNames 'mobile': IS.mobile(), 'transparent': @props.transparent, 'mobile': IS.mobile() }>
      <div id='layout'>
        <SelectMembership transparent={ @state.response.status isnt '' }/>
        <div id='form'>
          <div id='stripe' className={ classNames 'transparent': @state.response.status isnt '' }>
            <img src='./img/pictures/powered_by_stripe_black.png' alt='Stripe'/>
          </div>
          {
            if @state.response.status isnt ''
              <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                {
                  if @state.response.status is 'ready'
                    <p>{ @state.response.message }</p>
                  else
                    <div id='loading'></div>
                }
              </div>
          }
          <h1 className={ classNames 'transparent': @state.response.status isnt '' }>Start your membership now:</h1>
          <input id='name' className={ classNames 'left_form', 'transparent': @state.response.status isnt '', 'invalid': @state.form.nameValid is no } type='text' name='name' placeholder='Cardholder name...' value={ @state.form.name } onChange={ @typeName } onBlur={ @validateName }/>
          <input id='card_number' className={ classNames 'left_form', 'transparent': @state.response.status isnt '', 'invalid': @state.form.card_numberValid is no } type='text' name='card_number' placeholder='Card number' value={ @state.form.card_number } onChange={ @typeCardNumber } onBlur={ @validateCardNumber }/>
          <input id='cvv' className={ classNames 'left_form', 'transparent': @state.response.status isnt '', 'invalid': @state.form.cvvValid is no } type='text' name='cvv' placeholder='CVV' value={ @state.form.cvv } onChange={ @typeCvv } onBlur={ @validateCvv }/>
          <input id='exp_date' className={ classNames 'left_form', 'transparent': @state.response.status isnt '', 'invalid': @state.form.exp_dateValid is no } type='text' name='exp_date' placeholder='MM/YY' value={ @state.form.exp_date } onChange={ @typeExpDate } onBlur={ @validateExpDate }/>
          <input id='postal_code' className={ classNames 'left_form', 'transparent': @state.response.status isnt '', 'invalid': @state.form.postal_codeValid is no } type='text' name='postal_code' placeholder='Postal code' value={ @state.form.postal_code } onChange={ @typePostalCode } onBlur={ @validatePostalCode } onKeyPress={ @keyPress }/>
          <button className={ classNames 'button_blue', 'transparent': @state.response.status isnt '' } onClick={ @submitCard }>Continue</button>
          <p className={ classNames 'transparent': @state.response.status isnt '' }>*By submitting this form, you agree to our <span className='cursor_pointer' onClick={ @openPage.bind this, 'terms_of_service' }>Terms of Service</span> and <span className='cursor_pointer' onClick={ @openPage.bind this, 'privacy_policy' }>Privacy Policy</span>.</p>
        </div>
      </div>
    </div>



export default outfit RegisterForm
