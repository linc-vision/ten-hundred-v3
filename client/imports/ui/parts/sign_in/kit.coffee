import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import $ from 'jquery'

import classNames from 'classnames'

import Animations from '/client/imports/ui/animations/kit'

import { VideoBg } from '../kit'

import './kit.styl'



SignIn = class extends Component
  constructor: (props) ->
    super props
    @state =
      email: ''
      emailValid: null
      password: ''
      passwordValid: null
      error: no
      response:
        status: ''
        message: ''



  componentDidMount: =>
    @props.app.changePage 'sign_in'
    if @props.app.user.profile.email
      @setState
        email: @props.app.user.profile.email
      , =>
        @validateEmail()
        $('#SignIn #form input').eq(1).focus()



  typeEmail: (e) =>
    @setState
      email: e.target.value

  validateEmail: =>
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    @setState
      emailValid: emailReg.test @state.email

  typePassword: (e) =>
    @setState
      password: e.target.value

  validatePassword: =>
    @setState
      passwordValid: @state.password.length > 5

  validateAll: =>
    emailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    @setState
      emailValid: emailReg.test @state.email
      passwordValid: @state.password.length > 5

  openPage: (page) =>
    Animations.global.toggleForm =>
      if page is 'sign_in'
        @props.history.push '/sign-in'
      else
        @props.history.push '/home'

  keyPress: (e) =>
    if e.key is 'Enter'
      @login()

  login: =>
    @validateAll()
    setTimeout =>
      if @state.emailValid and @state.passwordValid
        @setState
          response:
            status: 'loading'
            message: ''
        , =>
          Meteor.loginWithPassword @state.email, @state.password, (err, res) =>
            if err
              @hideResponse()
              console.log err
              @setState
                error: yes
              , =>
                setTimeout () =>
                  @setState
                    error: no
                , 1000
            else
              @setState
                error: no
              , =>
                @hideResponse()
                Animations.global.changePage =>
                  @props.app.signIn()
                  @props.history.push '/new-request'
    , 100

  loginWithGoogle: =>
    Meteor.loginWithGoogle { requestPermissions: ['email'] }, (err, res) =>
      if err
        if err.errorType isnt 'Accounts.LoginCancelledError'
          if err.error is 403 then err.message = 'Email already exists'
          @setState
            response:
              status: 'ready'
              message: err.message
      else
        @hideResponse()
        Animations.global.changePage =>
          @props.app.signIn()
          @props.history.push '/new-request'

  loginWithFacebook: =>
    Meteor.loginWithFacebook { requestPermissions: ['email'] }, (err, res) =>
      if err
        if err.errorType isnt 'Accounts.LoginCancelledError'
          if err.error is 403 then err.message = 'Email already exists'
          if err.error is 405
            @hideResponse()
            @props.app.setFBSession err.reason
            @props.app.openModal 'no_email'
          else
            @setState
              response:
                status: 'ready'
                message: err.message
      else
        @hideResponse()
        Animations.global.changePage =>
          @props.app.signIn()
          @props.history.push '/new-request'

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''




  render: =>
    <div id='SignIn' className={ if IS.mobile() then 'mobile' }>
      {
        if @props.mobile
          <div id='video_bg'>
            <VideoBg/>
          </div>
      }
      <div id='layout'>
        <div id='form' className={ classNames 'error': @state.error }>
          {
            if @state.response.status isnt ''
              <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                {
                  if @state.response.status is 'ready'
                    <p>{ @state.response.message }</p>
                  else
                    <div id='loading'></div>
                }
              </div>
          }
          <h1 className={ classNames 'transparent': @state.response.status isnt '' }>Sign in:</h1>
          <input className={ classNames 'transparent': @state.response.status isnt '', 'right_form', 'invalid': @state.emailValid is no or @state.error } type='text' name='email' placeholder='Type your email here...' value={ @state.email } onChange={ @typeEmail } onBlur={ @validateEmail }/>
          <input className={ classNames 'transparent': @state.response.status isnt '', 'right_form', 'invalid': @state.passwordValid is no or @state.error } type='password' name='password' placeholder='Your password...' value={ @state.password } onChange={ @typePassword } onBlur={ @validatePassword } onKeyPress={ @keyPress }/>
          <button className={ classNames 'transparent': @state.response.status isnt '', 'button_blue' } onClick={ @login }>Login</button>
          <div id='socials' className={ classNames 'transparent': @state.response.status isnt '' }>
            <div id='or'>
              <div id='left' className='line'></div>
              <p>Or sign in with:</p>
                <div id='right' className='line'></div>
            </div>
            <div id='google' className={ classNames 'socials', 'cursor_pointer' } onClick={ @loginWithGoogle }>
              <div id='icon'></div>
              <p>Google</p>
            </div>
            <div id='facebook' className={ classNames 'socials', 'cursor_pointer' } onClick={ @loginWithFacebook }>
              <div id='icon'></div>
              <p>Facebook</p>
            </div>
          </div>
        </div>
        <button className={ classNames 'transparent': @state.response.status isnt '', 'button_white' } onClick={ @openPage.bind this, 'home' }>Back</button>
      </div>
    </div>



export default outfit SignIn
