import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import $ from 'jquery'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



SelectMembership = class extends Component
  constructor: (props) ->
    super props
    @state =
      active_plan: @props.app.register_form.active_plan
      cashback:
        cruise: @props.app.register_form.cashback.cruise
        ski: @props.app.register_form.cashback.ski
      enterPromo: no
      promo: ''
      promoSuccess: no



  selectMembership: (plan) =>
    @setState
      active_plan: plan
      cashback:
        cruise: '14%'
        ski: '10%'
    , =>
      @props.app.selectMembership plan, @state.promo

  showPromo: =>
    @setState
      enterPromo: yes
    , =>
      setTimeout () =>
        $('#SelectMembership #promo input').focus()
      , 300

  typePromo: (e) =>
    if not @state.promoSuccess
      @setState
        promo: e.target.value.toUpperCase()
      , =>
        @props.app.selectMembership @state.active_plan, @state.promo

  keyPress: (e) =>
    if not @state.promoSuccess
      if e.key is 'Enter'
        @checkPromo()

  checkPromo: =>
    Meteor.call 'checkPromo', @props.app.register_form.promo, (err, res) =>
      if err or res.status is 'F'
        $('#SelectMembership #promo input').addClass 'error'
        setTimeout () =>
          $('#SelectMembership #promo input').removeClass 'error'
        , 600
      else
        $('#SelectMembership #promo input').blur()
        @setState
          promoSuccess: yes

  openPage: =>
    Animations.global.changePage =>
      @props.app.signIn()
      @props.history.push '/new-request'



  render: =>
    <div id='SelectMembership' className={ classNames 'transparent': @props.transparent, 'mobile': IS.mobile() }>
      <div id='layout'>
        <div id='active_plan' className={ @state.active_plan }>
          <h3>Become a Member</h3>
          <h2>$149<span>/year</span></h2>
          <div id='cruise' className='element'>
            <div id='icon'></div>
            <h2>Ocean & River Cruises</h2>
            <p><span>{ @state.cashback.cruise }</span> Cashback</p>
          </div>
          <p id='limit' className={ @state.active_plan }>Annual rebate limit: <span className={ @state.active_plan }>{ if @state.active_plan is 'gold' then 'UNLIMITED' else '$1000' }</span></p>
          {
            if @state.active_plan is 'silver'
              if @state.enterPromo
                <div id='promo'>
                  <input className={ classNames 'left_form', 'success': @state.promoSuccess } type='text' name='promo-code' placeholder='Promo Code' value={ @state.promo } onChange={ @typePromo } onKeyPress={ @keyPress }/>
                </div>
              else
                <div id='promo'>
                  <p onClick={ @showPromo }>Enter a Coupon</p>
                </div>
          }
        </div>
      </div>
    </div>



export default outfit SelectMembership
