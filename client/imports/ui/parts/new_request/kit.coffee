import React, { Component } from 'react'
import { FilesCollection } from 'meteor/ostrio:files'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



Screenshots = new FilesCollection
  collectionName: 'screenshots'
  allowClientCode: yes
  onBeforeUpload: (file) ->
    if file.size <= 10485760 and /png|jpg|jpeg/i.test file.extension
      yes
    else
      'Please upload image, with size equal or less than 10MB'

Meteor.subscribe 'files.images.all'



NewRequest = class extends Component
  constructor: (props) ->
    super props
    @state =
      current_category: 'cruise'
      guaranteeVisible: no
      phone_number: ''
      phone_numberValid: null
      suppliers: @props.data.cruises
      form:
        price: ''
        priceValid: null
        supplier: ''
        supplierValid: null
        comment: ''
      screenshot_url: ''
      uploading: no



  componentDidMount: =>
    @props.app.changePage 'new_request'
    analytics.track 'Subscriber viewed "New Request" page'



  selectCategoty: (category) =>
    if category isnt 'cruise'
      category = 'cruise'
    if @state.screenshot_url isnt ''
      Meteor.call 'removeScreenshot', @state.screenshot_url

    @setState
      current_category: category
      suppliers: if category is 'cruise' then @props.data.cruises else @props.data.ski
      form:
        price: ''
        priceValid: null
        supplier: ''
        supplierValid: null
        comment: ''
      screenshot_url: ''
      uploading: no

  showGuarantee: =>
    @setState
      guaranteeVisible: yes

  hideGuarantee: =>
    @setState
      guaranteeVisible: no

  typePhone: (e) =>
    @setState
      phone_number: e.target.value.replace(/[^0-9()+]+/g, '')

  validatePhone: =>
    @setState
      phone_numberValid: @state.phone_number.length > 9 and @state.phone_number.length < 20

  confirmPhone: =>
    @validatePhone()
    if @state.phone_numberValid
      @props.app.addPhoneNumber @state.phone_number

  typePrice: (e) =>
    value = e.target.value.replace(/[^0-9.]/g, '').split '.'

    if Array.isArray value
      if value.length <= 2
        if value[0].length is 0 or value[0] is '0'
          value[0] = '1'
        else
          value[0] = value[0]
        if value.length is 2
          value[1] = value[1].slice(0, 2)
      else
        e.preventDefault()

    value = value.join('.')

    @setState
      form: {
        @state.form...
        price: value
      }

  validatePrice: =>
    @setState
      form: {
        @state.form...
        priceValid: parseFloat(@state.form.price).toFixed(2) > 0
      }

  typeComment: (e) =>
    @setState
      form: {
        @state.form...
        comment: e.target.value
      }

  selectSupplier: (e) =>
    @setState
      form: {
        @state.form...
        supplier: _.where(@state.suppliers, { title: e.target.value })[0]
      }

  validateSupplier: =>
    @setState
      form: {
        @state.form...
        supplierValid: @state.form.supplier isnt ''
      }

  validateAll: =>
    @setState
      form: {
        @state.form...
        priceValid: parseFloat(@state.form.price).toFixed(2) > 0
        supplierValid: @state.form.supplier isnt ''
      }

  fileUpload: (e) =>
    if e.currentTarget.files and e.currentTarget.files[0]
      upload = Screenshots.insert
        file: e.currentTarget.files[0]
        streams: 'dynamic'
        chunkSize: 'dynamic'
      , no

      upload.on 'start', (fileObject) =>
        @setState
          uploading: yes
        if @state.screenshot_url.length > 0
          Meteor.call 'removeScreenshot', @state.screenshot_url

      upload.on 'end', (error, fileObject) =>
        if error
          console.log error
        else
          setTimeout () =>
            @setState
              uploading: no
              screenshot_url:  Screenshots.findOne(fileObject._id).link()
          , 1200


      upload.start()

  sendRequest: =>
    @validateAll()

    if @state.form.priceValid and @state.form.supplierValid
      request =
        category: @state.current_category
        price: @state.form.price
        supplier: @state.form.supplier
        comment: @state.form.comment
        screenshot_url: @state.screenshot_url

      @props.app.createNewRequest request
      analytics.track 'Subscriber created new Request'
      @props.history.push '/all-requests'



  render: =>
    <div id='NewRequest' className={ classNames 'mobile': IS.mobile() }>
      {
        if @props.app.user.profile.email isnt '---'
          <div id='layout'>
            <div id='header'>
              <div id='logo'>
                <img src='/img/pictures/logo.png' alt='Ten Hundred Logo'/>
              </div>
              {
                if IS.mobile()
                  <div id='menu_button' onClick={ @props.app.openModal.bind this, 'mobile_menu' }>
                    <div id='icon'></div>
                    <div id='notification' className={ classNames 'hidden': @props.app.notifications.all_requests is 0 }>
                      {
                        if @props.app.notifications.all_requests <= 99
                          <p>{ if @props.app.notifications.all_requests > 0 then @props.app.notifications.all_requests }</p>
                        else
                          <p>99+</p>
                      }
                    </div>
                  </div>
              }
              <div id='total_saving'>
                {
                  if @props.app.user.profile.membership.plan.nickname is 'silver'
                    if parseInt(@props.app.user.profile.total_savings) >= 900 and parseInt(@props.app.user.profile.total_savings) < 1000
                      <p className='near'>Your savings are nearing the limit</p>
                    else if parseInt(@props.app.user.profile.total_savings) >= 1000
                      <p className='full'>Your savings at the limit</p>
                }
                <h2 id='label'>Total savings:</h2>
                <h2 id='amount' className={ classNames 'full': @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 1000, 'near': @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 900 }>{ "$#{ @props.app.user.profile.total_savings }" }</h2>
              </div>
            </div>
            <div id='content'>
              <div id='categories' className={ classNames 'middle': @state.current_category is '', 'shadow': @props.app.user.profile.phone_number is '' and @state.current_category isnt '' }>
                <div id='cruise' className={ classNames 'element' } onClick={ if @state.current_category isnt 'cruise' then @selectCategoty.bind this, 'cruise' }>
                  <div id='icon'></div>
                  <h2>Ocean & River Cruises</h2>
                </div>
              </div>
              <div id='form' className={ classNames 'hidden': @state.current_category is '', 'shadow': @props.app.user.profile.phone_number is '' and @state.current_category isnt '' }>
                <div id='note'>
                  <p><span>PLEASE NOTE:</span> You will not be charged by submitting a booking request. Once we received your request, we will prepare your booking details within 24 hours and send to you for final review. You will have the option then to select a preferred payment method and finalize your booking.</p>
                </div>
                <div id='upload'>
                  <input id='upload_file' type='file' accept='image/*,image/jpeg/png' onChange={ @fileUpload }/>
                  <label htmlFor='upload_file' className='cursor_pointer'>
                    <div id='input' className={ classNames 'loading': @state.uploading }>
                      <div id='icon' style={ if @state.screenshot_url isnt '' then { background: "url('#{ @state.screenshot_url }') center/cover no-repeat" } }></div>
                      {
                        if @state.screenshot_url is ''
                          <p>Upload<br/>a screenshot</p>
                        else
                          <p>CLICK HERE IF YOU NEED TO CHANGE YOUR SCREENSHOT</p>
                      }
                    </div>
                  </label>
                  {
                    if IS.mobile()
                      <p>*Please attach a screenshot of the product you are purchasing and include the price that you see.</p>
                  }
                </div>
                <div id='price'>
                  <input className={ classNames 'left_form', 'invalid': @state.form.priceValid is no } type='text' placeholder='Price...' value={ if @state.form.price.length > 0 then "$#{ @state.form.price }" else @state.form.price } onChange={ @typePrice } onBlur={ @validatePrice }/>
                </div>
                {
                  if not IS.mobile()
                    <p>*Please attach a screenshot of the product you are purchasing and include the price that you see.</p>
                }
                <div id='drop_list'>
                  <select className={ classNames 'invalid': @state.form.supplierValid is no } value={ if @state.form.supplier isnt '' then @state.form.supplier.title } onChange={ @selectSupplier } onBlur={ @validateSupplier }>
                    <option value=''>Supplier</option>
                    {
                      @state.suppliers.map (element, n) =>
                        <option key={ n } value={ element.title }>{ element.title }</option>
                    }
                  </select>
                  {
                    if IS.mobile()
                      <div id='picture' className={ classNames 'active': @state.guaranteeVisible } onClick={ if not @state.guaranteeVisible then @showGuarantee else @hideGuarantee }></div>
                    else
                      <div id='picture' onMouseOver={ @showGuarantee } onMouseOut={ @hideGuarantee }></div>
                  }
                  <div id='guarantee' className={ classNames 'hidden': not @state.guaranteeVisible } onClick={ @hideGuarantee }>
                    <p>We’re so confident that our members get the best deals on the planet that if our price can’t beat yours, we’ll send you a “tenhundred” ($1000 USD) check.*</p>
                  </div>
                </div>
                <div id='comment'>
                  <textarea className='left_form' rows='8' value={ @state.form.comment } onChange={ @typeComment } placeholder='Please enter additional details or anything you would like us to know for your booking.'/>
                </div>
              </div>
              <div id='send_request' className={ classNames 'hidden': @state.current_category is '', 'shadow': @props.app.user.profile.phone_number is '' and @state.current_category isnt '' }>
                <button className='button_blue' onClick={ @sendRequest }>Send Request</button>
              </div>
              {
                if @props.app.user.profile.phone_number is '' and @state.current_category isnt ''
                  <div id='ask'>
                    <div id='form'>
                      <div id='icon'></div>
                      <p>Please enter your preferred phone number for your bookings. You can later change this in the membership tab.</p>
                      <input className={ classNames 'left_form', 'invalid': @state.phone_numberValid is no } type='text' placeholder='Phone number...' value={ @state.phone_number } onChange={ @typePhone } onBlur={ @validatePhone }/>
                      <div id='confirm'>
                        <button className='button_blue' onClick={ @confirmPhone }>Confirm</button>
                      </div>
                    </div>
                  </div>
              }
            </div>
          </div>
      }
    </div>



export default outfit NewRequest
