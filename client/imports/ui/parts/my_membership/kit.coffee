import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import { SelectMembership, RegisterForm } from '../kit'

import Animations from '/client/imports/ui/animations/kit'

import classNames from 'classnames'

import './kit.styl'



MyMembership = class extends Component
  constructor: (props) ->
    super props
    @state =
      response:
        status: ''
        message: ''
      add_credit_card:
        adding: no
        form:
          name: ''
          nameValid: null
          card_number: ''
          card_numberValid: null
          cvv: ''
          cvvValid: null
          exp_date: ''
          exp_dateValid: null
          postal_code: ''
          postal_codeValid: null
      changing_phone_number: no
      phone_number: ''
      phone_numberValid: null



  componentDidMount: =>
    @props.app.changePage 'my_membership'
    analytics.track 'Subscriber viewed "My Membership" page'
    if @props.app.user.profile.email is '---'
      @props.app.openModal 'no_email'



  buyMembership: =>
    @setState
      response:
        status: 'loading'
        message: ''
    , =>
      if @props.app.register_form.promo
        plan =
          plan: @props.app.register_form.active_plan
          promo: @props.app.register_form.promo
      else
        plan = @props.app.register_form.active_plan

      Meteor.call 'subscribe', 'exist', null, plan, (err, res) =>
        if err or res.status is 'F'
          console.log err
          @setState
            response:
              status: 'ready'
              message: res.message.message
          , =>
            setTimeout () =>
              @hideResponse()
            , 4000
        else
          @hideResponse()
          @props.app.signIn()

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''

  addCreditCard: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        adding: yes
      }

  typeName: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          name: e.target.value.replace(/[^A-z ]+/g, '')
        }
      }

  validateName: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          nameValid: if @state.add_credit_card.form.name.length > 0 then yes else no
        }
      }

  typeCardNumber: (e) =>
    value = e.target.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ').trim()

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          card_number: if value.replace(/[^0-9]+/g, '').length <= 16 then value else @state.add_credit_card.form.card_number
        }
      }

  validateCardNumber: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          card_numberValid: if @state.add_credit_card.form.card_number.replace(/[^0-9]+/g, '').length >= 14 then yes else no
        }
      }

  typeCvv: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          cvv: if e.target.value.length <= 4 then e.target.value.replace(/[^0-9]+/g, '') else @state.add_credit_card.form.cvv
        }
      }

  validateCvv: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          cvvValid: if @state.add_credit_card.form.cvv.length >= 3 and @state.add_credit_card.form.cvv.length <= 4 then yes else no
        }
      }

  typeExpDate: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          exp_date: e.target.value.replace(/^([1-9]\/|[2-9])$/g, '0$1/').replace(/^(0[1-9]|1[0-2])$/g, '$1/').replace(/^([0-1])([3-9])$/g, '0$1/$2').replace(/^(\d)\/(\d\d)$/g, '0$1/$2').replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2').replace(/^([0]+)\/|[0]+$/g, '0').replace(/[^\d\/]|^[\/]*$/g, '').replace(/\/\//g, '/')
        }
      }

  validateExpDate: =>
    if @state.add_credit_card.form.exp_date
      current_year = new Date().getFullYear()
      selected_year = parseInt("20#{ @state.add_credit_card.form.exp_date.split('/')[1] }")
      valid = current_year < selected_year
    else
      valid = no

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          exp_dateValid: valid
        }
      }

  typePostalCode: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          postal_code: e.target.value.replace(/[^0-9]+/g, '')
        }
      }

  validatePostalCode: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          postal_codeValid: if @state.add_credit_card.form.postal_code.length > 4 then yes else no
        }
      }

  validateAll: =>
    current_year = new Date().getFullYear()
    selected_year = parseInt("20#{ @state.add_credit_card.form.exp_date.split('/')[1] }")

    valid = current_year < selected_year

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          nameValid: if @state.add_credit_card.form.name.length > 0 then yes else no
          card_numberValid: if @state.add_credit_card.form.card_number.replace(/[^0-9]+/g, '').length >= 14 then yes else no
          cvvValid: if @state.add_credit_card.form.cvv.length >= 3 and @state.add_credit_card.form.cvv.length <= 4 then yes else no
          exp_dateValid: valid
          postal_codeValid: if @state.add_credit_card.form.postal_code.length > 4 then yes else no
        }
      }

  submitCard: =>
    @validateAll()
    if @state.add_credit_card.form.nameValid and @state.add_credit_card.form.card_numberValid and @state.add_credit_card.form.cvvValid and @state.add_credit_card.form.exp_dateValid and @state.add_credit_card.form.postal_codeValid
      @setState
        response:
          status: 'loading'
          message: ''
      , =>
        credit_card =
          name: @state.add_credit_card.form.name
          card_number: @state.add_credit_card.form.card_number
          cvv: @state.add_credit_card.form.cvv
          exp_date: @state.add_credit_card.form.exp_date
          postal_code: @state.add_credit_card.form.postal_code

        Meteor.call 'addCreditCard', 'exist', @props.app.user.profile.customer_id, credit_card, (err, res) =>
          if err or res.status is 'F'
            @setState
              response:
                status: 'ready'
                message: res.message.message
            , =>
              setTimeout () =>
                if @state.response.status isnt '' and @state.response.message isnt ''
                  @hideResponse()
              , 8000
          else
            if res.status is 'S'
              @props.app.signIn()
              analytics.track 'Subscriber added a new Credit Card'
              @setState
                response:
                  status: ''
                  message: ''
                add_credit_card: {
                  @state.add_credit_card...
                  adding: no
                }

  makeCardDefault: (id) =>
    Meteor.call 'setDefaultCreditCard', id, (err, res) =>
      if err or res.status is 'F'
        console.log err
      else
        @props.app.signIn()
        analytics.track 'Subscriber changed default Credit Card'

  removeCreditCard: (id) =>
    Meteor.call 'removeCreditCard', id, (err, res) =>
      if err or res.status is 'F'
        console.log err
      else
        @props.app.signIn()
        analytics.track 'Subscriber removed a Credit Card'

  toggleChangePhoneNumber: =>
    @setState
      changing_phone_number: if @state.changing_phone_number then no else yes

  typePhone: (e) =>
    @setState
      phone_number: e.target.value.replace(/[^0-9()+]+/g, '')

  validatePhone: =>
    @setState
      phone_numberValid: @state.phone_number.length > 9 and @state.phone_number.length < 20

  confirmPhone: =>
    @validatePhone()
    if @state.phone_numberValid
      @props.app.addPhoneNumber @state.phone_number
      @setState
        phone_number: ''
      , =>
        analytics.track 'Subscriber added a Phone Number'
        @setState
          changing_phone_number: no



  render: =>
    <div id='MyMembership' className={ classNames 'mobile': IS.mobile() }>
      {
        if @props.app.user.profile.email isnt '---'
          <div id='layout'>
            <div id='header'>
              <div id='logo'>
                <img src='./img/pictures/logo.png' alt='Ten Hundred Logo'/>
              </div>
              {
                if IS.mobile()
                  <div id='menu_button' onClick={ @props.app.openModal.bind this, 'mobile_menu' }>
                    <div id='icon'></div>
                    <div id='notification' className={ classNames 'hidden': @props.app.notifications.all_requests is 0 }>
                      {
                        if @props.app.notifications.all_requests <= 99
                          <p>{ if @props.app.notifications.all_requests > 0 then @props.app.notifications.all_requests }</p>
                        else
                          <p>99+</p>
                      }
                    </div>
                  </div>
              }
              <div id='total_saving'>
                {
                  if @props.app.user.profile.membership.plan.nickname is 'silver'
                    if parseInt(@props.app.user.profile.total_savings) >= 900 and parseInt(@props.app.user.profile.total_savings) < 1000
                      <p className='near'>Your savings are nearing the limit</p>
                    else if parseInt(@props.app.user.profile.total_savings) >= 1000
                      <p className='full'>Your savings at the limit</p>
                }
                <h2 id='label'>Total savings:</h2>
                <h2 id='amount' className={ classNames 'full': @props.app.user.profile.membership.status is 'active' and @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 1000, 'near': @props.app.user.profile.membership.status is 'active' and @props.app.user.profile.membership.plan.nickname is 'silver' and parseInt(@props.app.user.profile.total_savings) >= 900 }>{ "$#{ @props.app.user.profile.total_savings }" }</h2>
              </div>
            </div>
            <div id='content' className={ classNames 'not_member': @props.app.user.profile.membership.status isnt 'active' }>
              {
                if @props.app.user.profile.membership.status is 'active'
                  <div id='plan'>
                    {
                      if @props.app.user.profile.membership.plan.nickname is 'silver'
                        <div id='upgrade' className='cursor_pointer' onClick={ @props.app.openModal.bind this, 'alert_upgrade' }></div>
                    }
                    <div id='left'>
                      <div id='icon' className={ @props.app.user.profile.membership.plan.nickname }></div>
                    </div>
                    <div id='right'>
                      <h1>{ "$#{ @props.app.user.profile.membership.plan.amount.toString().slice 0, -2 }" }<span>/year</span></h1>
                      <p>{ @props.app.user.profile.email }</p>
                    </div>
                  </div>
                else
                  <div id='plan' className='not_member'>
                    {
                      if @state.response.status isnt ''
                        <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                          {
                            if @state.response.status is 'ready'
                              <p>{ @state.response.message }</p>
                            else
                              <div id='loading'></div>
                          }
                        </div>
                    }
                    <RegisterForm transparent={ @state.response.status isnt '' }/>
                  </div>
              }
              {
                if @props.app.user.profile.membership.status is 'active'
                  <div id='options'>
                    <div id='left'>
                      <h2 id='label'>Next payment:</h2>
                      <h2 id='value'>{ moment(@props.app.user.profile.membership.current_period_end * 1000).format 'DD.MM.YYYY' }</h2>
                    </div>
                    <div id='right'>
                      <button className='button_grey' onClick={ @props.app.openModal.bind this, 'alert' }>Cancel<br/>Membership</button>
                    </div>
                  </div>
              }
              {
                if @props.app.user.profile.membership.status is 'active'
                  <div id='credit_cards'>
                    <h2>Payment methods:</h2>
                    {
                      @props.app.user.profile.credit_cards.map (credit_card, n) =>
                        <div key={ n } className='card'>
                          {
                            if credit_card.id is @props.app.user.profile.customer_data.default_source
                              <div id='default'>
                                <p>Default</p>
                              </div>
                            else
                              <div id='options' className='cursor_pointer'>
                                <p id='make_default' onClick={ @makeCardDefault.bind this, credit_card.id }>Make default</p>
                                <p id='remove' onClick={ @removeCreditCard.bind this, credit_card.id }>Delete card</p>
                              </div>
                          }
                          <div id='icon'>
                            {
                              if credit_card.brand is 'Visa'
                                <img src='./img/icons/visa.png' alt={ credit_card.brand }/>
                              else if credit_card.brand is 'MasterCard'
                                <img src='./img/icons/mastercard.png' alt={ credit_card.brand }/>
                              else if credit_card.brand is 'American Express'
                                <img src='./img/icons/american_express.png' alt={ credit_card.brand }/>
                              else if credit_card.brand is 'Diners Club'
                                <img src='./img/icons/diners_club.png' alt={ credit_card.brand }/>
                              else if credit_card.brand is 'Discover'
                                <img src='./img/icons/discover.png' alt={ credit_card.brand }/>
                              else
                                <img src='./img/icons/mastercard.png' alt={ credit_card.brand }/>
                            }
                          </div>
                          <div id='number'>
                            <h1>****</h1>
                            <h1>****</h1>
                            <h1>****</h1>
                            <h1>{ credit_card.last4 }</h1>
                          </div>
                          <div id='left'>
                            <p id='label'>Cardholder name:</p>
                            <h2 id='value'>{ credit_card.name }</h2>
                          </div>
                          <div id='right'>
                            <p id='label'>Expires</p>
                            <h2 id='value'>{ "#{ credit_card.exp_month }/#{ credit_card.exp_year }" }</h2>
                          </div>
                        </div>
                    }
                    {
                      if @props.app.user.profile.credit_cards.length <= 5
                        if @state.add_credit_card.adding is no
                          <div id='add_credit_card' className='cursor_pointer' onClick={ @addCreditCard }>
                            <p>+ Add another card</p>
                          </div>
                        else
                          <div id='credit_card_form'>
                            {
                              if @state.response.status isnt ''
                                <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
                                  {
                                    if @state.response.status is 'ready'
                                      <p>{ @state.response.message }</p>
                                    else
                                      <div id='loading'></div>
                                  }
                                </div>
                            }
                            <input id='name' className={ classNames 'right_form', 'transparent': @state.response.status isnt '', 'invalid': @state.add_credit_card.form.nameValid is no } type='text' name='name' placeholder='Cardholder name...' value={ @state.add_credit_card.form.name } onChange={ @typeName } onBlur={ @validateName }/>
                            <input id='card_number' className={ classNames 'right_form', 'transparent': @state.response.status isnt '', 'invalid': @state.add_credit_card.form.card_numberValid is no } type='text' name='card_number' placeholder='Card number' value={ @state.add_credit_card.form.card_number } onChange={ @typeCardNumber } onBlur={ @validateCardNumber }/>
                            <input id='cvv' className={ classNames 'right_form', 'transparent': @state.response.status isnt '', 'invalid': @state.add_credit_card.form.cvvValid is no } type='text' name='cvv' placeholder='CVV' value={ @state.add_credit_card.form.cvv } onChange={ @typeCvv } onBlur={ @validateCvv }/>
                            <input id='exp_date' className={ classNames 'right_form', 'transparent': @state.response.status isnt '', 'invalid': @state.add_credit_card.form.exp_dateValid is no } type='text' name='exp_date' placeholder='MM/YY' value={ @state.add_credit_card.form.exp_date } onChange={ @typeExpDate } onBlur={ @validateExpDate }/>
                            <input id='postal_code' className={ classNames 'right_form', 'transparent': @state.response.status isnt '', 'invalid': @state.add_credit_card.form.postal_codeValid is no } type='text' name='postal_code' placeholder='Postal code' value={ @state.add_credit_card.form.postal_code } onChange={ @typePostalCode } onBlur={ @validatePostalCode }/>
                            <button className={ classNames 'button_blue', 'transparent': @state.response.status isnt '' } onClick={ @submitCard }>Add card</button>
                          </div>
                    }
                  </div>
              }
              <div id='phone_number'>
                <div id='form'>
                  <div id='icon'></div>
                  {
                    if @props.app.user.profile.phone_number is ''
                      <p>Please enter your preferred phone number for your bookings. You can later change this here</p>
                    else
                      <h2>{ @props.app.user.profile.phone_number }</h2>
                  }
                  {
                    if @state.changing_phone_number or @props.app.user.profile.phone_number is ''
                      <input className={ classNames 'left_form', 'invalid': @state.phone_numberValid is no } type='text' placeholder='Change phone number...' value={ @state.phone_number } onChange={ @typePhone } onBlur={ @validatePhone }/>
                    else
                      <p id='change_phone_number' className='cursor_pointer' onClick={ @toggleChangePhoneNumber }>Change</p>
                  }
                  {
                    if @props.app.user.profile.phone_number is '' or @state.changing_phone_number
                      <div id='confirm'>
                        <button className='button_blue' onClick={ @confirmPhone }>Confirm</button>
                      </div>
                  }
                </div>
              </div>
            </div>
          </div>
      }
    </div>



export default outfit MyMembership
