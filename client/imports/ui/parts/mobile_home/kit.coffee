import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import { EmailForm, SignIn, CreatePasswordForm } from '../kit'

import './kit.styl'



MobileHome = class extends Component
  constructor: (props) ->
    super props
    @state =
      guaranteeVisible: no



  componentDidMount: =>
    if @props.history.location.pathname is '/'
      @props.app.changePage 'home'
    else if @props.history.location.pathname is '/cruises'
      @props.app.openModal 'cruise'

  componentDidUpdate: (prevProps) =>
    if @props.popup
      if not @props.app.modal.opened
        if @props.popup is 'cruises'
          @props.app.openModal 'cruise'



  changePage: (page) =>
    if page is 'cruises'
      @props.history.push '/cruises'



  openPage: (page) =>
    if page is 'cruises'
      @props.history.push '/cruises'
    else
      if page is 'sign_in'
        Animations.global.toggleForm =>
          @props.history.push '/sign-in'
      else
        Animations.global.changePage =>
          if page is 'terms_of_service'
            @props.history.push '/terms-of-service'
          else if page is 'privacy_policy'
            @props.history.push '/privacy-policy'

  showGuarantee: =>
    @setState
      guaranteeVisible: yes

  hideGuarantee: =>
    @setState
      guaranteeVisible: no



  render: =>
    <div id='MobileHome'>
      <div id='layout'>
        <div id='header'>
          <div id='logo'>
            <img src='./img/pictures/logo.png' alt='Ten Hundred Logo'/>
          </div>
          <div id='menu_button' onClick={ @props.app.openModal.bind this, 'mobile_menu' }>
            <div id='icon'></div>
          </div>
          <h3>The world’s first travel wholesale club</h3>
        </div>
        <div id='list'>
          <h2>Becoming a TenHundred club member<br/> is as simple as 1, 2, 3</h2>
          <div id='one' className='element'>
            <div className='icon'></div>
            <div>
              <h2>Register</h2>
              <p>Become a TenHundred member today.</p>
            </div>
          </div>
          <div id='two' className='element'>
            <div className='icon'></div>
            <div>
              <h2>Research</h2>
              <p>Look for your perfect vacation and send us the details. We’ll finish the booking and registration process for you.</p>
            </div>
          </div>
          <div id='three' className='element'>
            <div className='icon'></div>
            <div>
              <h2>Relax & Rejoice</h2>
              <p>Travel the world and get cash back from TenHundred when you return home.</p>
            </div>
          </div>
          <h2>Simple.</h2>
          <p id='sure'>Are you ready for extra cha-ching while you travel? Become a TenHundred member today!</p>
        </div>
        {
          if @props.app.page.id is 'register'
            <Switch>
              <Route exact path='/' render={ => <CreatePasswordForm mobile={ yes }/> }/>
              <Route path='/cruises' render={ => <CreatePasswordForm mobile={ yes }/> }/>
            </Switch>
          else
            <Switch>
              <Route exact path='/' render={ => <EmailForm mobile={ yes }/> }/>
              <Route path='/cruises' render={ => <EmailForm mobile={ yes }/> }/>
              <Route path='/sign-in' render={ => <SignIn mobile={ yes }/> }/>
            </Switch>
        }
        <div id='announcement'>
          <div id='badge'></div>
        </div>
        <div id='products'>
          <h1>Available Products:</h1>
          <div id='cruise' className='product'>
            <div id='header'>
              <div id='icon'></div>
              <h2>Ocean & River Cruises</h2>
            </div>
            <div id='list'>
              <div className='item'>
                <p id='title'>Royal Caribbean International</p>
                <p id='percentage'>14%</p>
              </div>
              <div className='item'>
                <p id='title'>Disney Cruises</p>
                <p id='percentage'>14%</p>
              </div>
              <div className='item'>
                <p id='title'>Carnival Cruise Line</p>
                <p id='percentage'>14%</p>
              </div>
              <div className='item'>
                <p id='title'>Scenic Cruises</p>
                <p id='percentage'>16%</p>
              </div>
            </div>
            <button className='button_blue' onClick={ @changePage.bind this, 'cruises' }>More</button>
          </div>
        </div>
        <div className='info'>
          <p><strong>TenHundred</strong> is for savvy travelers who love adventuring around the world as much as <strong>saving money</strong>.<br/>Thanks to our <strong>unique business model</strong>, the commissions that would normally go to third party agents when booking your favorite cruises now <strong>go to you!</strong><br/>Simply book your favorite adventures AND <strong>get money back</strong> while you play.</p>
        </div>
        <div id='annotation'>
          <div id='box'>
            <div id='badge'></div>
            <p>We’re so confident that our members get the best deals on the planet that if our price can’t beat yours, we’ll send you a “tenhundred” ($1000 USD) check.*</p>
            <p id='additional'>*Price is defined as the net package cost after rebate. The lower rate must be advertised on a competitor's website and available for booking at the time you contact us.</p>
          </div>
        </div>
        <div id='things'>
          <div className='thing'>
            <h1 id="number">1</h1>
            <img src="/img/icons/extra_money.png"/>
            <div id="text">
              <h3 id="title">The Price You See is the Price You Pay — And You Score Rebates!</h3>
              <p id="description">Similar to Expedia, Orbitz, or Hotwire, you’ll get the cheapest deals on the travel packages you want to book. Then… what’s different? With TenHundred, you also get awesome rebates that arrive straight into your bank account after your trip is done. Now that’s a nice perk to come home to.</p>
            </div>
          </div>
          <div className='thing'>
            <h1 id="number">2</h1>
            <img src="/img/icons/last_minute.png"/>
            <div id="text">
              <h3 id="title">Book Spontaneous Trips at the Last Minute.</h3>
              <p id="description">Sometimes, you just want to get away and get away fast. TenHundred features amazing last-minute deals that you can book, save, and still get money back.</p>
            </div>
          </div>
          <div className='thing'>
            <h1 id="number">3</h1>
            <img src="/img/icons/thanks.png"/>
            <div id="text">
              <h3 id="title">What’s Your Secret Sauce?</h3>
              <p id="description">Here’s what you might not know about the travel industry: every time you book a package through a third party site, someone somewhere is making money off of it. Why shouldn’t that someone be you? Thanks to our membership model, we take the commissions that travel agents normally make and turn that money right back over to you.</p>
            </div>
          </div>
        </div>
        <div id='footer'>
          <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'terms_of_service' } onClick={ if @props.app.page.id isnt 'terms_of_service' then @openPage.bind this, 'terms_of_service' }>Terms of Service</p>
          <p className={ classNames 'cursor_pointer', 'active': @props.app.page.id is 'privacy_policy' } onClick={ if @props.app.page.id isnt 'privacy_policy' then @openPage.bind this, 'privacy_policy' }>Privacy Policy</p>
          <p>{ "©#{ new Date().getFullYear() } Ten Hundred" }</p>
        </div>
      </div>
    </div>



export default outfit MobileHome
