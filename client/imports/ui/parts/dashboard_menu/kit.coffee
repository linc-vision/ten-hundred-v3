import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'



DashboardMenu = class extends Component
  constructor: (props) ->
    super props
    @state =
      notifications:
        all_requests: @props.app.notifications.all_requests or 0



  componentDidUpdate: (prevProps) =>
    if @props.app.notifications
      if not _.isEqual @props.app.notifications, prevProps.app.notifications
        @setState
          notifications:
            all_requests: @props.app.notifications.all_requests or 0



  openPage: (page) =>
    if page is 'new_request'
      if IS.mobile()
        @props.closeModal()
      @props.history.push '/new-request'
    else if page is 'all_requests'
      if IS.mobile()
        @props.closeModal()
      @props.history.push '/all-requests'
    else if page is 'my_membership'
      if IS.mobile()
        @props.closeModal()
      @props.history.push '/my-membership'



  render: =>
    <div id='DashboardMenu'>
      <div id='layout'>
        <div id='menu'>
          <div id='new_request' className={ classNames 'element', 'inactive': @props.app.user.profile.email is '---', 'cursor_pointer': @props.app.page.id isnt 'new_request', 'active': @props.app.page.id is 'new_request' } onClick={ if @props.app.page.id isnt 'new_request' then @openPage.bind this, 'new_request' }>
            <div id='icon'></div>
            <h2>New Request</h2>
          </div>
          <div id='all_requests' className={ classNames 'element', 'inactive': @props.app.user.profile.email is '---', 'cursor_pointer': @props.app.page.id isnt 'all_requests', 'active': @props.app.page.id is 'all_requests' } onClick={ if @props.app.page.id isnt 'all_requests' then @openPage.bind this, 'all_requests' }>
            <div id='icon'></div>
            <h2>All Requests</h2>
            <div id='notification' className={ classNames 'hidden': @state.notifications.all_requests is 0 }>
              {
                if @state.notifications.all_requests <= 99
                  <p>{ if @state.notifications.all_requests > 0 then @state.notifications.all_requests }</p>
                else
                  <p>99+</p>
              }
            </div>
          </div>
          {
            if @props.app.user.profile.email isnt '---'
              <div id='my_membership' className={ classNames 'element', @props.app.user.profile.membership.plan.nickname, 'canceled': @props.app.user.profile.membership.status isnt 'active', 'cursor_pointer': @props.app.page.id isnt 'my_membership', 'active': @props.app.page.id is 'my_membership' } onClick={ if @props.app.page.id isnt 'my_membership' then @openPage.bind this, 'my_membership' }>
                <div id='icon'></div>
                <h2>My Membership</h2>
              </div>
            else
              <div id='my_membership' className={ classNames 'element', 'canceled', 'cursor_pointer': @props.app.page.id isnt 'my_membership', 'active': @props.app.page.id is 'my_membership' } onClick={ if @props.app.page.id isnt 'my_membership' then @openPage.bind this, 'my_membership' }>
                <div id='icon'></div>
                <h2>My Membership</h2>
              </div>
          }
        </div>
      </div>
    </div>



export default outfit DashboardMenu
