import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import classNames from 'classnames'

import anime from 'animejs'

import $ from 'jquery'

import Animations from '/client/imports/ui/animations/kit'

import './kit.styl'

import { SearchBox } from '@loadup/react-google-places-autocomplete'



ModalAddPaymentMethod = class extends Component
  constructor: (props) ->
    super props
    @state =
      type: ''
      add_credit_card:
        adding: no
        form:
          name: ''
          nameValid: null
          card_number: ''
          card_numberValid: null
          cvv: ''
          cvvValid: null
          exp_date: ''
          exp_dateValid: null
          postal_code: ''
          postal_codeValid: null
      add_bank_account:
        adding: no
        form:
          routing: ''
          routingValid: null
          account: ''
          accountValid: null
          re_account: ''
          re_accountValid: null
          recipient: 'person'
          name: ''
          nameValid: null
      add_address:
        adding: no
        form:
          address: ''
          addressValid: null
      response:
        status: ''
        message: ''



  selectType: (type) =>
    @setState
      type: type
      add_credit_card:
        adding: no
        form:
          name: ''
          nameValid: null
          card_number: ''
          card_numberValid: null
          cvv: ''
          cvvValid: null
          exp_date: ''
          exp_dateValid: null
          postal_code: ''
          postal_codeValid: null

  hideResponse: =>
    if @state.response.status isnt ''
      @setState
        response:
          status: ''
          message: ''

  addCreditCard: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        adding: yes
      }

  typeName: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          name: e.target.value.replace(/[^A-z ]+/g, '')
        }
      }

  validateName: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          nameValid: if @state.add_credit_card.form.name.length > 0 then yes else no
        }
      }

  typeCardNumber: (e) =>
    value = e.target.value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ').trim()

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          card_number: if value.replace(/[^0-9]+/g, '').length <= 16 then value else @state.add_credit_card.form.card_number
        }
      }

  validateCardNumber: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          card_numberValid: if @state.add_credit_card.form.card_number.replace(/[^0-9]+/g, '').length is 16 then yes else no
        }
      }

  typeCvv: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          cvv: if e.target.value.length <= 4 then e.target.value.replace(/[^0-9]+/g, '') else @state.add_credit_card.form.cvv
        }
      }

  validateCvv: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          cvvValid: if @state.add_credit_card.form.cvv.length >= 3 and @state.add_credit_card.form.cvv.length <= 4 then yes else no
        }
      }

  typeExpDate: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          exp_date: e.target.value.replace(/^([1-9]\/|[2-9])$/g, '0$1/').replace(/^(0[1-9]|1[0-2])$/g, '$1/').replace(/^([0-1])([3-9])$/g, '0$1/$2').replace(/^(\d)\/(\d\d)$/g, '0$1/$2').replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2').replace(/^([0]+)\/|[0]+$/g, '0').replace(/[^\d\/]|^[\/]*$/g, '').replace(/\/\//g, '/')
        }
      }

  validateExpDate: =>
    if @state.add_credit_card.form.exp_date
      current_year = new Date().getFullYear() - 1
      selected_year = parseInt("20#{ @state.add_credit_card.form.exp_date.split('/')[1] }")
      valid = current_year < selected_year
    else
      valid = no

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          exp_dateValid: valid
        }
      }

  typePostalCode: (e) =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          postal_code: e.target.value.replace(/[^0-9]+/g, '')
        }
      }

  validatePostalCode: =>
    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          postal_codeValid: if @state.add_credit_card.form.postal_code.length > 4 then yes else no
        }
      }

  validateAll: =>
    current_year = new Date().getFullYear() - 1
    selected_year = parseInt("20#{ @state.add_credit_card.form.exp_date.split('/')[1] }")

    valid = current_year < selected_year

    @setState
      add_credit_card: {
        @state.add_credit_card...
        form: {
          @state.add_credit_card.form...
          nameValid: if @state.add_credit_card.form.name.length > 0 then yes else no
          card_numberValid: if @state.add_credit_card.form.card_number.replace(/[^0-9]+/g, '').length is 16 then yes else no
          cvvValid: if @state.add_credit_card.form.cvv.length >= 3 and @state.add_credit_card.form.cvv.length <= 4 then yes else no
          exp_dateValid: valid
          postal_codeValid: if @state.add_credit_card.form.postal_code.length > 4 then yes else no
        }
      }

  submitCard: =>
    @validateAll()
    if @state.add_credit_card.form.nameValid and @state.add_credit_card.form.card_numberValid and @state.add_credit_card.form.cvvValid and @state.add_credit_card.form.exp_dateValid and @state.add_credit_card.form.postal_codeValid
      @setState
        response:
          status: 'loading'
          message: ''
      , =>
        credit_card =
          name: @state.add_credit_card.form.name
          card_number: @state.add_credit_card.form.card_number
          cvv: @state.add_credit_card.form.cvv
          exp_date: @state.add_credit_card.form.exp_date
          postal_code: @state.add_credit_card.form.postal_code

        Meteor.call 'addCreditCard', 'exist', @props.app.user.profile.customer_id, credit_card, (err, res) =>
          if err or res.status is 'F'
            @setState
              response:
                status: 'ready'
                message: if res.message.message is 'Your card number is incorrect.' then 'Your card was declined. Please try again.' else res.message.message
            , =>
              setTimeout () =>
                if @state.response.status isnt '' and @state.response.message isnt ''
                  @hideResponse()
              , 8000
          else
            if res.status is 'S'
              @props.app.signIn()
              analytics.track 'Subscriber added a new Credit Card'
              @props.closeModal()

  typeRouting: (e) =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          routing: e.target.value.replace(/[^0-9]+/g, '')
        }
      }

  validateRouting: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          routingValid: if @state.add_bank_account.form.routing.length is 9 then yes else no
        }
      }

  typeAccount: (e) =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          account: e.target.value.replace(/[^0-9]+/g, '')
        }
      }

  validateAccount: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          accountValid: if @state.add_bank_account.form.account.length > 0 then yes else no
        }
      }

  typeReAccount: (e) =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          re_account: e.target.value.replace(/[^0-9]+/g, '')
        }
      }

  validateReAccount: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          re_accountValid: @state.add_bank_account.form.account is @state.add_bank_account.form.re_account
        }
      }

  toggleRecipient: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          recipient: if @state.add_bank_account.form.recipient is 'person' then 'business' else 'person'
        }
      }

  typeBankName: (e) =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          name: e.target.value.replace(/[^A-z ]+/g, '')
        }
      }

  validateBankName: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          nameValid: if @state.add_bank_account.form.name.length > 0 then yes else no
        }
      }

  validateBankAll: =>
    @setState
      add_bank_account: {
        @state.add_bank_account...
        form: {
          @state.add_bank_account.form...
          routingValid: if @state.add_bank_account.form.routing.length is 9 then yes else no
          accountValid: if @state.add_bank_account.form.account.length > 0 then yes else no
          re_accountValid: @state.add_bank_account.form.account is @state.add_bank_account.form.re_account
          nameValid: if @state.add_bank_account.form.name.length > 0 then yes else no
        }
      }

  submitAccount: =>
    @validateBankAll()
    if @state.add_bank_account.form.routingValid and @state.add_bank_account.form.accountValid and @state.add_bank_account.form.re_accountValid and @state.add_bank_account.form.nameValid
      @setState
        response:
          status: 'loading'
          message: ''
      , =>
        account =
          routing: @state.add_bank_account.form.routing
          account: @state.add_bank_account.form.account
          recipient: @state.add_bank_account.form.recipient
          name: @state.add_bank_account.form.name

        Meteor.call 'addBankAccount', Meteor.userId(), account, (err, res) =>
          if err or res.status is 'F'
            @setState
              response:
                status: 'ready'
                message: res.message
            , =>
              setTimeout () =>
                if @state.response.status isnt '' and @state.response.message isnt ''
                  @hideResponse()
              , 8000
          else
            if res.status is 'S'
              @props.app.signIn()
              analytics.track 'Subscriber added a new Bank Account'
              @props.closeModal()

  typeAddress: (address) =>
    @setState
      add_address: {
        @state.add_address...
        form: {
          @state.add_address.form...
          address: address
        }
      }

  submitAddress: =>
    if IS.object @state.add_address.form.address
      @setState
        response:
          status: 'loading'
          message: ''
      , =>
        Meteor.call 'addAddress', Meteor.userId(), @state.add_address.form.address, (err, res) =>
          if err or res.status is 'F'
            @setState
              response:
                status: 'ready'
                message: res.message
            , =>
              setTimeout () =>
                if @state.response.status isnt '' and @state.response.message isnt ''
                  @hideResponse()
              , 8000
          else
            if res.status is 'S'
              @props.app.signIn()
              analytics.track 'Subscriber added a new Address'
              @props.closeModal()
    else
      @setState
        add_address: {
          @state.add_address...
          form: {
            @state.add_address.form...
            addressValid: no
          }
        }



  render: =>
    <div id='ModalAddPaymentMethod' className={ classNames 'mobile': IS.mobile() }>
      <div id='layout'>
        {
          if @state.response.status isnt ''
            <div id='response' onClick={ if @state.response.status is 'ready' then @hideResponse }>
              {
                if @state.response.status is 'ready'
                  <p>{ @state.response.message }</p>
                else
                  <div id='loading'></div>
              }
            </div>
        }
        <div id='content' className={ classNames 'transparent': @state.response.status isnt '', 'scrollbar_hidden' }>
          <div id='types' className={ classNames 'middle': @props.method is 'add_cashback_method' and @state.type is '' }>
            {
              if @props.method is 'add_payment_method'
                <div id='credit_card' className={ classNames 'element' }>
                  <div id='icon'></div>
                  <h2>Credit Card</h2>
                </div>
              else if @props.method is 'add_cashback_method'
                [ <div key='1' id='check' className={ classNames 'element', 'live', 'active': @state.type is 'check' } onClick={ if @state.type isnt 'check' then @selectType.bind this, 'check' }>
                  <div id='icon'></div>
                  <h2>Check</h2>
                </div>,
                <div key='2' id='bank_account' className={ classNames 'element', 'live', 'active': @state.type is 'account' } onClick={ if @state.type isnt 'account' then @selectType.bind this, 'account' }>
                  <div id='icon'></div>
                  <h2>Direct Deposit</h2>
                </div> ]
            }
          </div>
          <div id='form' className='scrollbar_hidden'>
            {
              if @props.method is 'add_payment_method' or @props.method is 'add_cashback_method' and @state.type isnt ''
                <div id='layout'>
                  {
                    if @props.method is 'add_payment_method'
                      <div id='credit_card_form'>
                        <input id='name' className={ classNames 'right_form', 'invalid': @state.add_credit_card.form.nameValid is no } type='text' name='name' placeholder='Cardholder name...' value={ @state.add_credit_card.form.name } onChange={ @typeName } onBlur={ @validateName }/>
                        <input id='card_number' className={ classNames 'right_form', 'invalid': @state.add_credit_card.form.card_numberValid is no } type='text' name='card_number' placeholder='Card number' value={ @state.add_credit_card.form.card_number } onChange={ @typeCardNumber } onBlur={ @validateCardNumber }/>
                        <input id='cvv' className={ classNames 'right_form', 'invalid': @state.add_credit_card.form.cvvValid is no } type='text' name='cvv' placeholder='CVV' value={ @state.add_credit_card.form.cvv } onChange={ @typeCvv } onBlur={ @validateCvv }/>
                        <input id='exp_date' className={ classNames 'right_form', 'invalid': @state.add_credit_card.form.exp_dateValid is no } type='text' name='exp_date' placeholder='MM/YY' value={ @state.add_credit_card.form.exp_date } onChange={ @typeExpDate } onBlur={ @validateExpDate }/>
                        <input id='postal_code' className={ classNames 'right_form', 'invalid': @state.add_credit_card.form.postal_codeValid is no } type='text' name='postal_code' placeholder='Postal code' value={ @state.add_credit_card.form.postal_code } onChange={ @typePostalCode } onBlur={ @validatePostalCode }/>
                        <button className={ classNames 'button_blue' } onClick={ @submitCard }>Add card</button>
                      </div>
                    else if @props.method is 'add_cashback_method'
                      if @state.type is 'account'
                        <div id='bank_account_form'>
                          <h2>Please enter your bank account information for direct deposit.</h2>
                          <div id='recipient'>
                            <p id='label'>Recipient</p>
                            <p id='person'>Person</p>
                            <div id='checkbox' className={ classNames 'cursor_pointer', @state.add_bank_account.form.recipient } onClick={ @toggleRecipient }>
                              <div id='circle'></div>
                            </div>
                            <p id='business'>Business</p>
                          </div>
                          <input id='routing' className={ classNames 'right_form', 'invalid': @state.add_bank_account.form.routingValid is no } type='text' name='routing' placeholder='Routing #' value={ @state.add_bank_account.form.routing } onChange={ @typeRouting } onBlur={ @validateRouting }/>
                          <input id='account' className={ classNames 'right_form', 'invalid': @state.add_bank_account.form.accountValid is no } type='text' name='account' placeholder='Account #' value={ @state.add_bank_account.form.account } onChange={ @typeAccount } onBlur={ @validateAccount }/>
                          <input id='re_account' className={ classNames 'right_form', 'invalid': @state.add_bank_account.form.re_accountValid is no } type='text' name='re_account' placeholder='Re-enter Account #' value={ @state.add_bank_account.form.re_account } onChange={ @typeReAccount } onBlur={ @validateReAccount }/>
                          <input id='name' className={ classNames 'right_form', 'invalid': @state.add_bank_account.form.nameValid is no } type='text' name='name' placeholder='Full Name' value={ @state.add_bank_account.form.name } onChange={ @typeBankName } onBlur={ @validateBankName }/>
                          <button className={ classNames 'button_blue' } onClick={ @submitAccount }>Add account</button>
                        </div>
                      else
                        <div id='address_form'>
                          <h2>Please enter the mailing address where you would like your cash back check to be sent.</h2>
                          <SearchBox id='address' className={ classNames 'right_form', 'invalid': @state.add_address.form.addressValid is no } onPlaceChanged={ @typeAddress } placeholder='Type your address...'/>
                          <button className={ classNames 'button_blue' } onClick={ @submitAddress }>Add address</button>
                        </div>
                  }
                </div>
            }
          </div>
        </div>
      </div>
    </div>



export default outfit ModalAddPaymentMethod
