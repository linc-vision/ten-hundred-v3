import React, { Component } from 'react'

import { outfit } from '/client/imports/redux/outfit'

import './kit.styl'



ModalAdminRequest = class extends Component
  constructor: (props) ->
    super props
    @state =
      price: ''
      cashback: ''
      date: ''
      comment: ''



  componentWillUnmount: =>
    @props.app.adminUnsetCurrentRequest()



  typePrice: (e) =>
    value = e.target.value.replace(/[^0-9.]/g, '').split '.'

    if Array.isArray value
      if value.length <= 2
        if value[0].length is 0 or value[0] is '0'
          value[0] = '1'
        else
          value[0] = value[0]
        if value.length is 2
          value[1] = value[1].slice(0, 2)
      else
        e.preventDefault()

    value = value.join('.')

    @setState
      price: value

  typeCashback: (e) =>
    value = e.target.value.replace(/[^0-9.]/g, '').split '.'

    if Array.isArray value
      if value.length <= 2
        if value[0].length is 0 or value[0] is '0'
          value[0] = '1'
        else
          value[0] = value[0]
        if value.length is 2
          value[1] = value[1].slice(0, 2)
      else
        e.preventDefault()

    value = value.join('.')

    @setState
      cashback: value

  typeDate: (e) =>
    value = e.target.value.replace(/[^0-9/]+/g, '')
    length = e.target.value.length

    if length is 2 or length is 5
      value += '/'

    @setState
      date: value

  typeComment: (e) =>
    @setState
      comment: e.target.value

  send: =>
    data = @state

    Meteor.call 'sendConfirmation', @props.app.current_request, data,  (err, res) =>
      if err
        console.log err
      else if res.status is 'F'
        console.log res.message
      else
        Meteor.call 'adminViewRequest', @props.app.current_request._id
        @props.closeModal()

  complete: =>
    data = @state

    Meteor.call 'completeRequest', @props.app.current_request,  (err, res) =>
      if not err
        Meteor.call 'adminViewRequest', @props.app.current_request._id
        @props.closeModal()



  render: =>
    <div id='ModalAdminRequest'>
      <div id='layout'>
        <div id='header'>
          <div id='icon' className={ @props.app.current_request.category }></div>
          {
            if @props.app.current_request.category is 'cruise'
              <h2 id='title'>Ocean & River<br/>Cruises</h2>
            else if @props.app.current_request.category is 'ski'
              <h2 id='title'>Ski Tickets<br/>& Passes</h2>
          }
        </div>
        <div id='content' className='scrollbar_hidden'>
          <div id='user_details'>
            <div id='screenshot' className={ if @props.app.current_request.screenshot_url.length > 0 then 'cursor_pointer' else 'no_image' }>
              {
                if @props.app.current_request.screenshot_url.length > 0
                  <a href={ @props.app.current_request.screenshot_url } target='_blank'>
                    <img src={ @props.app.current_request.screenshot_url }/>
                  </a>
                else
                  <div id='icon'></div>
              }
            </div>
            <div id='info'>
              <h2 id='supplier'>{ @props.app.current_request.supplier.title }</h2>
              <h2 id='price'>{ "$#{ @props.app.current_request.price }" }</h2>
              <p id='comment'>{ @props.app.current_request.comment }</p>
            </div>
          </div>
          {
            if @props.app.current_request.status is 'processing'
              <div id='form' className='scrollbar_hidden'>
                <div id='layout'>
                  <input className='left_form' id='price' type='text' name='price' placeholder='PRICE' value={ if @state.price.length > 0 then "$#{ @state.price }" else @state.price } onChange={ @typePrice }/>
                  <div id='cashback'>
                    {
                      user = _.findWhere(@props.data.users, { _id: @props.app.current_request.owner.id })
                      if user.profile.membership.plan.nickname is 'silver'
                        <div id='limit'>
                          <div id='icon'></div>
                          <p>{ "$#{ user.profile.total_savings }/" }<span>$1000</span></p>
                        </div>
                    }
                    <input className='left_form' id='cashback' type='text' name='cashback' placeholder='CASHBACK' value={ if @state.cashback.length > 0 then "$#{ @state.cashback }" else @state.cashback } onChange={ @typeCashback }/>
                  </div>
                  <input className='left_form' id='date' type='text' name='date' placeholder='DATE' maxLength='10' value={ @state.date } onChange={ @typeDate }/>
                  <textarea className='left_form' name='comment' rows='6' placeholder='Comment...' value={ @state.comment } onChange={ @typeComment }/>
                  <button className='button_blue' onClick={ @send }>Send confirmation</button>
                </div>
              </div>
            else
              [<div key='1' id='payment_method'>
                <h2>Payment method:</h2>
                {
                  user_card = _.filter(_.findWhere(@props.data.users, { _id: @props.app.current_request.owner.id }).profile.credit_cards, (card) => card.stripe.id is @props.app.current_request.confirmation.payment_method.id)[0]
                  user_card.card_number = user_card.card_number.replace(/[^0-9]+/g, '')
                  <div id='card'>
                    <div id='icon'>
                      {
                        if @props.app.current_request.confirmation.payment_method.brand is 'Visa'
                          <img src='/img/icons/visa.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                        else if @props.app.current_request.confirmation.payment_method.brand is 'MasterCard'
                          <img src='/img/icons/mastercard.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                        else if @props.app.current_request.confirmation.payment_method.brand is 'American Express'
                          <img src='/img/icons/american_express.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                        else if @props.app.current_request.confirmation.payment_method.brand is 'Diners Club'
                          <img src='/img/icons/diners_club.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                        else if @props.app.current_request.confirmation.payment_method.brand is 'Discover'
                          <img src='/img/icons/discover.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                        else
                          <img src='/img/icons/mastercard.png' alt={ @props.app.current_request.confirmation.payment_method.brand }/>
                      }
                    </div>
                    <div id='number'>
                      <h1>{ user_card.card_number.slice 0, 4 }</h1>
                      <h1>{ user_card.card_number.slice 4, 8 }</h1>
                      <h1>{ user_card.card_number.slice 8, 12 }</h1>
                      <h1>{ user_card.card_number.slice 12, 16 }</h1>
                    </div>
                    <div id='left'>
                      <p id='label'>Cardholder name:</p>
                      <h2 id='value'>{ @props.app.current_request.confirmation.payment_method.name }</h2>
                    </div>
                    <div id='right'>
                      <p id='label'>Expires</p>
                      <h2 id='value'>{ "#{ @props.app.current_request.confirmation.payment_method.exp_month }/#{ @props.app.current_request.confirmation.payment_method.exp_year }" }</h2>
                    </div>
                    <div id='bleft'>
                      <p id='label'>Zipcode</p>
                      <h2 id='value'>{ user_card.postal_code }</h2>
                    </div>
                    <div id='bright'>
                      <p id='label'>CVV:</p>
                      <h2 id='value'>{ user_card.cvv }</h2>
                    </div>
                  </div>
                }
              </div>,
              <div key='2' id='cashback_method'>
                <h2>Cashback method:</h2>
                {
                  if @props.app.current_request.confirmation.cashback_method.object is 'account'
                    <div id='account'>
                      <div id='recipient'>
                        <p>{ @props.app.current_request.confirmation.cashback_method.recipient }</p>
                      </div>
                      <div id='icon'>
                        <img src='/img/icons/bank_account.png'/>
                      </div>
                      <div id='account'>
                        <h1>{ @props.app.current_request.confirmation.cashback_method.account }</h1>
                      </div>
                      <div id='left'>
                        <p id='label'>Name:</p>
                        <h2 id='value'>{ @props.app.current_request.confirmation.cashback_method.name }</h2>
                      </div>
                      <div id='right'>
                        <p id='label'>Routing</p>
                        <h2 id='value'>{ @props.app.current_request.confirmation.cashback_method.routing }</h2>
                      </div>
                    </div>
                  else if @props.app.current_request.confirmation.cashback_method.object is 'address'
                    <div id='address'>
                      <div id='icon'>
                        <img src='/img/icons/check.png'/>
                      </div>
                      <div id='account'>
                        <h1>{ if @props.app.current_request.confirmation.cashback_method.original[0].formatted_address then @props.app.current_request.confirmation.cashback_method.original[0].formatted_address else @props.app.current_request.confirmation.cashback_method.original[0].name }</h1>
                      </div>
                    </div>
                }
                {
                  if @props.app.current_request.status is 'confirmed'
                    <button className='button_blue' onClick={ @complete }>Set as completed</button>
                }
              </div>]
          }
        </div>
      </div>
    </div>



export default outfit ModalAdminRequest
